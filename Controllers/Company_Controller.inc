<?php
	include_once "../Services/Company_Service.inc";
	include_once "../Data/Company.inc";

	class Company_Controller {

		private $company_Service;

		function Company_Controller() {
       		$this->company_Service = new Company_Service();
   		}

		public function get_All_Companies() {
			return $this->company_Service->get_All();
		}

		private function view_To_Object($name, $address, $web, $phone, $rfc) {
			$company = new Company();

			$company->set_name($name);
			$company->set_address($address);
			$company->set_phone($phone);
			$company->set_web_Page($web);
			$company->set_rfc($rfc);

			return $company;
		}

	}
?>