<?php
	include_once "../Services/Court_Service.inc";
	include_once "../Data/Court.inc";

	class Court_Controller {

		private $court_Service;

		function Court_Controller() {
       		$this->court_Service = new Court_Service();
   		}

		public function create_Court($companyId, $name, $description, $latitude, $length) {
			return $this->court_Service->create($this->view_To_Object($companyId, $name, $description, $latitude, $length));
		}

		public function get_All_Courts($company) {
			return $this->court_Service->get_By_Company($company);
		}

		private function view_To_Object($companyId, $name, $description, $latitude, $length) {
			$court_Entity = new Court();

			$court_Entity->set_name($name);
			$court_Entity->set_company($companyId);
			$court_Entity->set_description($description);
			$court_Entity->set_latitude($latitude);
			$court_Entity->set_length($length);

			return $court_Entity;
		}

	}
?>