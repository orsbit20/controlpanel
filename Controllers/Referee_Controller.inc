<?php
	include_once "../Services/Referee_Service.inc";
	include_once "../Data/Referee.inc";

	class Referee_Controller {

		private $referee_Service;

		function Referee_Controller() {
       		$this->referee_Service = new Referee_Service();
   		}

		public function create_Referee($name, $address, $phone, $web, $rfc) {
			return $this->referee_Service->create($this->view_To_Object($name, $address, $phone, $web, $rfc));
		}

		public function get_All_Referees() {
			return $this->referee_Service->get_All();
		}

		private function view_To_Object($name, $address, $phone, $web, $rfc) {
			$referee_Entity = new Referee();

			$referee_Entity->set_name($name);
			$referee_Entity->set_address($address);
			$referee_Entity->set_phone($phone);
			$referee_Entity->set_web_Page($web);
			$referee_Entity->set_rfc($rfc);

			return $referee_Entity;
		}

	}
?>