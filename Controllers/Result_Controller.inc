<?php
	include_once "../Services/Result_Service.inc";
	include_once "../Data/Result.inc";
	include_once "../Data/Result_Details.inc";

	class Result_Controller {

		private $result_Service;

		function Result_Controller() {
       		$this->result_Service = new Result_Service();
   		}

		public function create_Result($match, $local_Goals, $foreign_Goals, $extra1, $extra2, $player, $yellow, $red, $goals) {
			$details = array();

			for ($i = 0; $i < count($player) ; $i++) {
    			array_push($details, $this->view_To_Object_Details($extra2[$i], $player[$i], $yellow[$i], $red[$i], $goals[$i]));
			}
			
			return $this->result_Service->create_Results($this->view_To_Object($match, $local_Goals, $foreign_Goals, $extra1), $details);
		}

		public function get_All_Results() {
			return $this->result_Service->get_All();
		}

		private function view_To_Object($match, $local_Goals, $foreign_Goals, $extra1) {
			$result = new Result();

			$result->set_match($match);
			$result->set_local_Goals($local_Goals);
			$result->set_foreign_Goals($foreign_Goals);
			$result->set_extras($extra1);

			return $result;
		}

		private function view_To_Object_Details($extra2, $player, $yellow, $red, $goals) {
			$detail = new Result_Details();

			$detail->set_extras($extra2);
			$detail->set_player($player);
			$detail->set_yellow_Cards($yellow);
			$detail->set_red_Cards($red);
			$detail->set_goals($goals);

			return $detail;
		}

	}
?>