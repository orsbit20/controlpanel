<?php
	include_once "../Services/Player_Service.inc";
	include_once "../Data/Player.inc";

	class Player_Controller {

		private $player_Service;

		function Player_Controller() {
       		$this->player_Service = new Player_Service();
   		}

		public function create_Player($team, $name, $number, $photo) {
			return $this->player_Service->create($this->view_To_Object($team, $name, $number, $photo));
		}

		public function get_All_Players() {
			return $this->player_Service->get_All();
		}

		public function get_All_Players_By_Team($team) {
			return $this->player_Service->get_Players_By_Team($team);
		}

		public function get_All_Players_By_League($league) {
			return $this->player_Service->get_Players_By_League($league);
		}

		private function view_To_Object($team, $name, $number, $photo) {
			$player_Entity = new Player();

			$player_Entity->set_team($team);
			$player_Entity->set_name($name);
			$player_Entity->set_number($number);
			$player_Entity->set_photo($photo);

			return $player_Entity;
		}

	}
?>