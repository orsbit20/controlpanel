<?php
	include_once "../Services/News_Service.inc";
	include_once "../Data/News.inc";

	class News_Controller {

		private $news_Service;

		function News_Controller() {
       		$this->news_Service = new News_Service();
   		}

		public function create_News($title, $content, $publish_Date, $photo) {
			return $this->news_Service->create($this->view_To_Object($title, $content, $publish_Date, $photo));
		}

		public function get_All_News() {
			return $this->news_Service->get_All();
		}

		public function get_All_News_By_Company($company) {
			return $this->news_Service->get_All_By_Company($company);
		}

		private function view_To_Object($title, $content, $publish_Date, $photo) {
			$news_Entity = new News();

			$news_Entity->set_title($title);
			$news_Entity->set_content($content);
			$news_Entity->set_publish_Date($publish_Date);
			$news_Entity->set_photo($photo);

			return $news_Entity;
		}

	}
?>