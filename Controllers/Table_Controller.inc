<?php
	include_once "../Services/Table_Service.inc";
	include_once "../Data/Table.inc";

	class Table_Controller {

		private $table_Service;

		function Table_Controller() {
       		$this->table_Service = new Table_Service();
   		}

		public function get_Table($league) {
			return $this->table_Service->get_Table_By_League($league);
		}

		public function get_Table_Goals($league) {
			return $this->table_Service->get_Table_Goals_By_League($league);
		}

	}
?>