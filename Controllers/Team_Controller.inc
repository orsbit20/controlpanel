<?php
	include_once "../Services/Team_Service.inc";
	include_once "../Data/Team.inc";

	class Team_Controller {

		private $team_Service;

		function Team_Controller() {
       		$this->team_Service = new Team_Service();
   		}

		public function create_Team($league, $name, $logo, $description, $contact, $phone, $creation_date) {
			return $this->team_Service->create($this->view_To_Object($league, $name, $logo, $description, $contact, $phone, $creation_date));
		}

		public function get_All_Teams() {
			return $this->team_Service->get_All();
		}

		public function get_All_Teams_By_League($league) {
			return $this->team_Service->get_All_By_League($league);
		}

		public function get_Teams_By_Match($match) {
			return $this->team_Service->get_Teams_By_Match($match);
		}

		private function view_To_Object($league, $name, $logo, $description, $contact, $phone, $creation_date) {
			$team_Entity = new Team();

			$team_Entity->set_league($league);
			$team_Entity->set_name($name);
			$team_Entity->set_logo($logo);
			$team_Entity->set_description($description);
			$team_Entity->set_contact_Name($contact);
			$team_Entity->set_phone($phone);
			$team_Entity->set_registry_Date($creation_date);

			return $team_Entity;
		}

	}
?>