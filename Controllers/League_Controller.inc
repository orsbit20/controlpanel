<?php
	include_once "../Services/League_Service.inc";
	include_once "../Data/League.inc";

	class League_Controller {

		private $league_Service;

		function League_Controller() {
       		$this->league_Service = new League_Service();
   		}

		public function create_League($company, $name, $description, $prices, $dates, $day) {
			return $this->league_Service->create($this->view_To_Object($company, $name, $description, $prices, $dates, $day));
		}

		public function get_All_Leagues($company) {
			return $this->league_Service->get_All_By_Company($company);
		}

		private function view_To_Object($company, $name, $description, $prices, $dates, $day) {
			$league_Entity = new League();

			$league_Entity->set_company($company);
			$league_Entity->set_name($name);
			$league_Entity->set_description($description);
			$league_Entity->set_prices($prices);
			$league_Entity->set_dates($dates);
			$league_Entity->set_day($day);

			return $league_Entity;
		}

	}
?>