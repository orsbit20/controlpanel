<?php
	include_once "../Services/User_Service.inc";
	include_once "../Data/User_Panel.inc";

	class Login_Controller {

		private $user_Service;

		function Login_Controller() {
       		$this->user_Service = new User_Service();
   		}

		public function login($user, $pass) {
			return $this->user_Service->get_By_User_And_Pass($user, $pass);
		}

		public function create_User($user, $pass, $type, $company) {
			return $this->user_Service->create($this->view_To_Object($user, $pass, $type, $company));
		}

		private function view_To_Object($user, $pass, $type, $company) {
			$user_Entity = new User_Panel();

			$user_Entity->set_user($user);
			$user_Entity->set_pass($pass);
			$user_Entity->set_type($type);
			$user_Entity->set_company($company);

			return $user_Entity;
		}

	}
?>