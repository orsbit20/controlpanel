<?php
	include_once "../Services/Match_Service.inc";
	include_once "../Data/Match.inc";

	class Match_Controller {

		private $match_Service;

		function Match_Controller() {
       		$this->match_Service = new Match_Service();
   		}

		public function create_Match($league, $number, $local_Team, $foreign_Team, $court, $referee, $date) {
			return $this->match_Service->create($this->view_To_Object($league, $number, $local_Team, $foreign_Team, $court, $referee, $date));
		}

		public function get_All_Matches() {
			return $this->match_Service->get_All();
		}

		public function get_All_Matches_By_League($league) {
			return $this->match_Service->get_All_By_League($league);
		}

		private function view_To_Object($league, $number, $local_Team, $foreign_Team, $court, $referee, $date) {
			$match = new Match();

			$match->set_league($league);
			$match->set_number($number);
			$match->set_local_Team($local_Team);
			$match->set_foreign_Team($foreign_Team);
			$match->set_court($court);
			$match->set_referee($referee);
			$match->set_date($date);

			return $match;
		}

	}
?>