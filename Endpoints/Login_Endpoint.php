<?php
/*
  Endpoint que recibe peticiones por parte de login page
*/
  	include_once "../Controllers/Login_Controller.inc";

	extract ($_REQUEST);

	$user = $_POST['user'];
	$pass = $_POST['pass'];

	$login = new Login_Controller();

	$user = $login->login($user, $pass);

	if($user != null) {

		session_start();

		$_SESSION['user'] = $_POST['user'];
		$_SESSION['pass'] = $_POST['pass'];
		$_SESSION['company'] = $user->get_company();

		header('Location: ../Views/home.html');

	}

?>