<?php

  	include_once "../Controllers/League_Controller.inc";

  	session_start();

  	$company = $_SESSION['company'];

	$league_Controller = new League_Controller();

	echo json_encode($league_Controller->get_All_Leagues($company));

?>