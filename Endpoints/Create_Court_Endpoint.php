<?php
/*
  Endpoint que recibe peticiones por parte del formulario de registro
*/
  	include_once "../Controllers/Court_Controller.inc";

  	session_start();

	extract ($_REQUEST);

	$name = $_POST['name'];
	$companyId = $_SESSION['company'];
	$description = $_POST['description'];
	$latitude = $_POST['latitude'];
	$length = $_POST['length'];

	$court_controller = new Court_Controller();

	$court_controller->create_Court($companyId, $name, $description, $latitude, $length);

	header('Location: ../Views/court-view.html');

?>