<?php
/*
  Endpoint que recibe peticiones por parte del formulario de registro
*/
  	include_once "../Controllers/Match_Controller.inc";

	extract ($_REQUEST);

	$league = $_POST['league'];
	$number = $_POST['number'];
	$local_Team = $_POST['local_Team'];
	$foreign_Team = $_POST['foreign_Team']; 
	$court = $_POST['court'];
	$referee = $_POST['referee'];
	$date = $_POST['date'];

	$match_controller = new Match_Controller();

	$match_controller->create_Match($league, $number, $local_Team, $foreign_Team, $court, $referee, $date);

	header('Location: ../Views/matches.php?success=true');

?>