<?php

  	include_once "../Controllers/Match_Controller.inc";

  	extract ($_REQUEST);

  	$league = $_GET['league'];

	$match_Controller = new Match_Controller();

	echo json_encode($match_Controller->get_All_Matches_By_League($league));

?>