<?php
/*
  Endpoint que recibe peticiones por parte del formulario de registro
*/
  	include_once "../Controllers/Referee_Controller.inc";

	extract ($_REQUEST);

	$name = $_POST['name'];
	$address = $_POST['address'];
	$phone = $_POST['phone'];
	$web = $_POST['web'];
	$rfc = $_POST['rfc'];

	$referee_controller = new Referee_Controller();

	$referee_controller->create_Referee($name, $address, $phone, $web, $rfc);

	header('Location: ../Views/referee-view.html');

?>