<?php
/*
  Endpoint que recibe peticiones por parte del formulario de registro
*/
  	include_once "../Controllers/Player_Controller.inc";

	extract ($_REQUEST);

	$team = $_POST['team'];
	$name = $_POST['name'];
	$number = $_POST['number']; 
	$photo = $_FILES['photo'];

	$player_controller = new Player_Controller();

	$player_controller->create_Player($team, $name, $number, $photo);

	header('Location: ../Views/player-view.html');

?>