<?php
/*
  Endpoint que recibe peticiones por parte del formulario de registro
*/
  	include_once "../Controllers/Team_Controller.inc";

	extract ($_REQUEST);

	$league = $_POST['league'];
	$name = $_POST['name'];
	$logo = $_FILES['logo'];
	$contact = $_POST['contact']; 
	$description = $_POST['description'];
	$phone = $_POST['phone'];
	$creation_date = $_POST['creation_date'];

	$team_controller = new Team_Controller();

	$team_controller->create_Team($league, $name, $logo, $description, $contact, $phone, $creation_date);

	header('Location: ../Views/team-view.html');

?>