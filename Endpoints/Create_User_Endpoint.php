<?php
/*
  Endpoint que recibe peticiones por parte del formulario de registro
*/
  	include_once "../Controllers/Login_Controller.inc";

	extract ($_REQUEST);

	$user = $_POST['user'];
	$pass = $_POST['pass'];
	$type = $_POST['type'];
	$company = $_POST['companyId'];

	$login = new Login_Controller();

	$user = $login->create_User($user, $pass, $type, $company);

	session_start();
	
	$_SESSION['user'] = $user;
	$_SESSION['pass'] = $pass;
	$_SESSION['company'] = $company;

	header('Location: ../Views/index.php');

?>