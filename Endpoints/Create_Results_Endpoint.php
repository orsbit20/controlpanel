<?php
/*
  Endpoint que recibe peticiones por parte del formulario de registro
*/
  	include_once "../Controllers/Result_Controller.inc";

	extract ($_REQUEST);

	$match = $_POST["matchId"];
	$local_Goals = $_POST['local_Goals'];
	$foreign_Goals = $_POST['foreign_Goals'];
	$extra1 = $_POST['extra1']; 

	$extra2 = $_POST['extra2'];
	$player = $_POST['player'];
	$yellow = $_POST['yellow'];
	if(isset($_POST['red'])) {
		$red = $_POST['red'];
	}else {
		$red = array();
		for ($i=0; $i < count($player); $i++) { 
			array_push($red, 0);
		}
	}
	$goals = $_POST['goals'];

	$result_controller = new Result_Controller();

	$result_controller->create_Result($match, $local_Goals, $foreign_Goals, $extra1, $extra2, $player, $yellow, $red, $goals);

	header('Location: ../Views/results.php?success=true');

?>