<?php

  	include_once "../Controllers/Court_Controller.inc";

  	session_start();

  	$company = $_SESSION['company'];

	$court_Controller = new Court_Controller();

	echo json_encode($court_Controller->get_All_Courts($company));

?>