<?php
/*
  Endpoint que recibe peticiones por parte del formulario de registro
*/
  	include_once "../Controllers/League_Controller.inc";

	extract ($_REQUEST);

	session_start();

	$company = $_SESSION['company'];
	$name = $_POST['name'];
	$prices = $_POST['prices']; 
	$description = $_POST['description'];
	$dates = $_POST['dates'];
	$day = $_POST['day'];

	$league_controller = new League_Controller();

	$league_controller->create_League($company, $name, $description, $prices, $dates, $day);

	header('Location: ../Views/league-view.html');

?>