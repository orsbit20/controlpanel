<?php

  	include_once "../Controllers/News_Controller.inc";

  	session_start();

  	$company = $_SESSION['company'];

	$controller = new News_Controller();

	echo json_encode($controller->get_All_News_By_Company($company));

?>