<?php
	include_once "../Helpers/Config_Helper.inc";

	$config_Helper = new Config_Helper();

	$configuration = $config_Helper->get_DB_Configuration();

	$host = $configuration["connection"]["host"];
	$user = $configuration["connection"]["user"];
	$pass = $configuration["connection"]["pass"];
	$dbname = $configuration["connection"]["dbName"];

	$link = mysql_connect($host, $user) or
		die("Connection error, I can't connect to the DB: ".mysql_error());

	mysql_select_db($dbname);

?>