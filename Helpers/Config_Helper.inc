<?php

	class Config_Helper {

		public function get_Table_Name($name) {
			$data = $this->config_Binding();
			return $data["tables"][$name];
		}

		public function get_DB_Configuration() {
			$data = $this->config_Binding();
			return $data;
		}

		private function config_Binding() {
			return parse_ini_file("../Configuration/Init_Config.ini",true);
		}
	}
?>