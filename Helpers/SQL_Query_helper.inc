<?php

	class SQL_Query_Helper {

		private $SELECT_ALL_QUERY = "SELECT * FROM ";
		private $SELECT = "SELECT ";
		private $FROM = " FROM ";
		private $WHERE = " WHERE ";
		private $AND = " AND ";
		private $ACTIVE = "ACTIVO = 1";
		private $INNER_JOIN = " INNER JOIN ";
		private $ON = " ON ";
		private $INSERT = "INSERT INTO ";
		private $ID = "ID";

		public function select_All_From($table) {
			return $this->SELECT_ALL_QUERY.$table;
		}

		public function select_All_From_Active($table) {
			return $this->SELECT_ALL_QUERY.$table.$this->WHERE.$this->ACTIVE;
		}

		public function select_All_From_Where_Active($table, $map) {
			$query = $this->do_Select_All_From_Where($table, $map);
			return $query.$this->AND.$this->ACTIVE;
		}

		public function select_All_From_Where($table, $map) {
			return $this->do_Select_All_From_Where($table, $map);
		}

		public function select_All_From_Inner_Join_Where_Active($table, $inner_Table, $get_Array, $map) {
			$query = $this->do_select_All_From_Inner_Join_Where($table, $inner_Table, $get_Array, $map);
			return $query.$this->AND.$table.".".$this->ACTIVE;
		}

		public function insert($table, $map) {
			$query = $this->INSERT.$table." ";
			$query = $this->do_Insert_Query($query, $map);
			return $query;
		}

		private function do_Insert_Query($query, $map) {
			$query = $query."(";
			$index = 0;
			$queryValues = "VALUES (";
			foreach ($map as $key => $value) {
				$query = $query.$key;
				$queryValues = $queryValues.$this->transform_Value_For_Query($value);
				if($index == count($map)-1) {
					$query = $query.") ";
					$queryValues = $queryValues.") ";
				}else {
					$query = $query.", ";
					$queryValues = $queryValues.", ";
				}
				$index = $index + 1;
			}

			$query = $query.$queryValues;
			return $query;
		}

		private function transform_Value_For_Query($value) {
			if(is_string($value)) {
				$value = "'".$value."'";
			}
			return $value;
		}

		private function do_Select_All_From_Where($table, $map) {
			$query = $this->SELECT_ALL_QUERY.$table;
			return $this->create_Where_Values($query, $map);
		}

		private function do_select_All_From_Inner_Join_Where($table, $inner_Table, $get_Array, $map) {
			$query = $this->create_Get_Values($this->SELECT, $get_Array).$table;
			$query = $query.$this->INNER_JOIN.$inner_Table;
			$query = $query.$this->create_On_Values($table, $inner_Table);
			return $this->create_Where_Values($query, $map);
		}

		private function create_Get_Values($query, $array) {
			$index = 0;
			foreach ($array as $key) {
				$query = $query.$key;
				if($index < count($array)-1) {
					$query = $query.", ";
				}
				$index = $index + 1;
			}
			return $query.$this->FROM;
		}

		private function create_Where_Values($query, $map) {
			$query = $query.$this->WHERE;
			$and_Alive = false;
			if(count($map) > 1) $and_Alive = true;
			$index = 0;
			foreach ($map as $key => $value) {
				$query = $query.$key." = "."'".$value."' ";
				if($and_Alive && $index < count($map)-1) {
					$query = $query.$this->AND;
				}
				$index = $index + 1;
			}
			return $query;
		}

		private function create_On_Values($child_Table, $father_Table) {
			$father_Id = $father_Table.".".$this->ID;
			$child_Id = $child_Table.".".$this->ID."_".$father_Table;

			return $this->ON.$father_Id." = ".$child_Id." ";
		}
	}

?>