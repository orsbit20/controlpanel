<?php
	include_once "Service_Interface.inc";
	include_once "Helpers/JSON_Data_Helper.inc";
	include_once "../DAO/Team_DAO.inc";
	include_once "../DAO/League_DAO.inc";
	include_once "../DAO/Referee_DAO.inc";
	include_once "../DAO/Court_DAO.inc";
	include_once "../DAO/Match_DAO.inc";
	include_once "../DAO/Result_DAO.inc";
	include_once "../Data/Match.inc";

	class Match_Service implements Service_Interface {

		private $JSON_Helper;

		private $team_DAO;

		private $league_DAO;

		private $referee_DAO;

		private $court_DAO;

		private $result_DAO;

		private $match_DAO;

		function Match_Service(){
			$this->JSON_Helper = new JSON_Data_Helper();
			$this->team_DAO = new Team_DAO();
			$this->league_DAO = new League_DAO();
			$this->referee_DAO = new Referee_DAO();
			$this->court_DAO = new Court_DAO();
			$this->result_DAO = new Result_DAO();
			$this->match_DAO = new Match_DAO();
		}

		public function create($object) {
			$match = $this->match_DAO->save($object);
			return $this->JSON_Helper->transform_Entity_To_JSON($match->serialize_JSON());
		}

		public function get_One($id) {
			//Implementar funcionalidad
		}

		public function get_All() {
			$result = $this->match_DAO->find_All();

			$array_result = array();

			foreach($result as $value) {
				$value = $this->generate_Fancy_Match($value);

				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		public function get_All_By_League($league) {
			$result = $this->match_DAO->find_By_League($league);

			$array_result = array();

			foreach($result as $value) {
				$value = $this->generate_Fancy_Match($value);

				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		public function modify($entidad) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_By_Name($nombre) {
			// TODO: Implementar funcionalidad
		}

		private function generate_Fancy_Match($entity) {
			$league = $this->league_DAO->find_By_Id($entity->get_league());
			$local_Team = $this->team_DAO->find_By_Id($entity->get_local_Team());
			$foreign_Team = $this->team_DAO->find_By_Id($entity->get_foreign_Team());
			$court = $this->court_DAO->find_By_Id($entity->get_court());
			$referee = $this->referee_DAO->find_By_Id($entity->get_referee());
			$result = $this->result_DAO->find_By_Match($entity->get_id());

			$entity->set_league($league->get_name());
			$entity->set_local_Team($local_Team->get_name());
			$entity->set_foreign_Team($foreign_Team->get_name());
			$entity->set_court($court->get_name());
			$entity->set_referee($referee->get_name());
			$entity->set_local_Goals($result->get_local_Goals());
			$entity->set_foreign_Goals($result->get_foreign_Goals());

			$entity->set_local_Logo(base64_encode($local_Team->get_logo()));
			$entity->set_foreign_Logo(base64_encode($foreign_Team->get_logo()));

			return $entity;
		}

		private function validate_Photo($photo) {
			if($photo['error'] > 0) {
				return false;
			}else {
				$type_Allowed = array("image/jpg", "image/jpeg", "image/png");
				$size_limit = 16384;

				if(in_array($photo['type'], $type_Allowed) && $photo['size'] <= 1024 * $size_limit) {
					return true;
				}
			}
		}

		private function convert_Photo_To_String($photo) {
			$temp_image = $photo["tmp_name"];
			$type = $photo["type"];

			$fp = fopen($temp_image, 'r+b');
			$data = fread($fp, filesize($temp_image));
			fclose($fp);

			return mysql_escape_string($data);
		}

	}
?>