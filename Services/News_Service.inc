<?php
	include_once "Service_Interface.inc";
	include_once "Helpers/JSON_Data_Helper.inc";
	include_once "../DAO/News_DAO.inc";
	include_once "../Data/News.inc";

	class News_Service implements Service_Interface {

		private $JSON_Helper;

		private $news_DAO;

		function News_Service(){
			$this->JSON_Helper = new JSON_Data_Helper();
			$this->news_DAO = new News_DAO();
		}

		public function create($object) {
			if(!$this->validate_Photo($object->get_photo())) {
				return null;
			}else {
				$object->set_photo($this->convert_Photo_To_String($object->get_photo()));
				$news = $this->news_DAO->save($object);
				return $this->JSON_Helper->transform_Entity_To_JSON($news->serialize_JSON());
			}
		}

		public function get_One($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_All() {
			$result = $this->news_DAO->find_All();

			$array_result = array();

			foreach($result as $value) {

				$value->set_photo(base64_encode($value->get_photo()));

				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		public function get_All_By_Company($company) {
			$result = $this->news_DAO->find_By_Company($company);

			$array_result = array();

			foreach($result as $value) {

				$value->set_photo(base64_encode($value->get_photo()));

				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		public function modify($entidad) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_By_Name($nombre) {
			// TODO: Implementar funcionalidad
		}

		private function validate_Photo($photo) {
			if($photo['error'] > 0) {
				return false;
			}else {
				$type_Allowed = array("image/jpg", "image/jpeg", "image/png");
				$size_limit = 16384;
				if(in_array($photo['type'], $type_Allowed) && $photo['size'] <= 1024 * $size_limit) {
					return true;
				}
			}
		}

		private function convert_Photo_To_String($photo) {
			$temp_image = $photo["tmp_name"];
			$type = $photo["type"];

			$fp = fopen($temp_image, 'r+b');
			$data = fread($fp, filesize($temp_image));
			fclose($fp);

			return mysql_escape_string($data);
		}

	}
?>