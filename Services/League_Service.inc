<?php
	include_once "Service_Interface.inc";
	include_once "Helpers/JSON_Data_Helper.inc";
	include_once "../DAO/League_DAO.inc";
	include_once "../Data/League.inc";

	class League_Service implements Service_Interface {

		private $JSON_Helper;

		private $league_DAO;

		function League_Service(){
			$this->JSON_Helper = new JSON_Data_Helper();
			$this->league_DAO = new League_DAO();
		}

		public function create($object) {
			$league = $this->league_DAO->save($object);
			return $this->JSON_Helper->transform_Entity_To_JSON($league->serialize_JSON());
		}

		public function get_One($id) {
			$result = $this->league_DAO->find_By_Id($id);

			return $this->JSON_Helper->transform_Entity_To_JSON($result->serialize_JSON());
		}

		public function get_All() {
			$result = $this->league_DAO->find_All();

			$array_result = array();

			foreach($result as $value) {
				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		public function get_All_By_Company($company) {
			$result = $this->league_DAO->find_By_Company($company);

			$array_result = array();

			foreach($result as $value) {
				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		public function modify($entidad) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_By_Name($nombre) {
			// TODO: Implementar funcionalidad
		}

	}
?>