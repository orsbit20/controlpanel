<?php
	include_once "Service_Interface.inc";
	include_once "Helpers/JSON_Data_Helper.inc";
	include_once "../DAO/Referee_DAO.inc";
	include_once "../Data/Referee.inc";

	class Referee_Service implements Service_Interface {

		private $JSON_Helper;

		private $referee_DAO;

		function Referee_Service(){
			$this->JSON_Helper = new JSON_Data_Helper();
			$this->referee_DAO = new Referee_DAO();
		}

		public function create($object) {
			$referee = $this->referee_DAO->save($object);
			return $this->JSON_Helper->transform_Entity_To_JSON($referee->serialize_JSON());
		}

		public function get_One($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_All() {
			$result = $this->referee_DAO->find_All();

			$array_result = array();

			foreach($result as $value) {
				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		public function modify($entidad) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_By_Name($nombre) {
			// TODO: Implementar funcionalidad
		}

	}
?>