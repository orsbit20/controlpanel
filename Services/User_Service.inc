<?php
	include_once "Service_Interface.inc";
	include_once "Helpers/JSON_Data_Helper.inc";
	include_once "../DAO/User_Panel_DAO.inc";
	include_once "../Data/User_Panel.inc";

	class User_Service implements Service_Interface {

		private $JSON_Helper;

		private $user_DAO;

		function User_Service(){
			$this->JSON_Helper = new JSON_Data_Helper();
			$this->user_DAO = new User_Panel_DAO();
		}

		public function create($object) {
			$user = $this->user_DAO->save($object);
			return $this->JSON_Helper->transform_Entity_To_JSON($user->serialize_JSON());
		}

		public function get_One($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_All() {
			// TODO: Implementar funcionalidad
		}

		public function modify($entidad) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_By_Name($nombre) {
			// TODO: Implementar funcionalidad
		}

		public function get_By_User_And_Pass($user, $pass) {
			$user = $this->user_DAO->find_By_User_And_Pass($user, $pass);
			return $user;
		}
	}
?>