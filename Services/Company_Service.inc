<?php
	include_once "Service_Interface.inc";
	include_once "Helpers/JSON_Data_Helper.inc";
	include_once "../DAO/Company_DAO.inc";
	include_once "../Data/Company.inc";

	class Company_Service implements Service_Interface {

		private $JSON_Helper;

		private $company_DAO;

		function Company_Service(){
			$this->JSON_Helper = new JSON_Data_Helper();
			$this->company_DAO = new Company_DAO();
		}

		public function create($object) {
			// TODO: Implementar funcionalidad
		}

		public function get_One($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_All() {
			$result = $this->company_DAO->find_All();

			$array_result = array();

			foreach($result as $value) {
				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		public function modify($entidad) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_By_Name($nombre) {
			// TODO: Implementar funcionalidad
		}
	}
?>