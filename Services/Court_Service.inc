<?php
	include_once "Service_Interface.inc";
	include_once "Helpers/JSON_Data_Helper.inc";
	include_once "../DAO/Court_DAO.inc";
	include_once "../Data/Court.inc";

	class Court_Service implements Service_Interface {

		private $JSON_Helper;

		private $court_DAO;

		function Court_Service(){
			$this->JSON_Helper = new JSON_Data_Helper();
			$this->court_DAO = new Court_DAO();
		}

		public function create($object) {
			$court = $this->court_DAO->save($object);
			return $this->JSON_Helper->transform_Entity_To_JSON($court->serialize_JSON());
		}

		public function get_One($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_All() {
			$result = $this->court_DAO->find_All();

			$array_result = array();

			foreach($result as $value) {
				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		public function get_By_Company($company) {
			$result = $this->court_DAO->find_By_Company($company);

			$array_result = array();

			foreach($result as $value) {
				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		public function modify($entidad) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_By_Name($nombre) {
			// TODO: Implementar funcionalidad
		}

	}
?>