<?php

	class JSON_Data_Helper {

		public function transform_Entity_To_JSON($entity) {
			return str_replace("\\","", json_encode($entity, JSON_UNESCAPED_SLASHES));
		}
	}
?>