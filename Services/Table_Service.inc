<?php
	include_once "Service_Interface.inc";
	include_once "Helpers/JSON_Data_Helper.inc";
	include_once "../DAO/Team_DAO.inc";
	include_once "../DAO/League_DAO.inc";
	include_once "../DAO/Match_DAO.inc";
	include_once "../DAO/Player_DAO.inc";
	include_once "../DAO/Result_DAO.inc";
	include_once "../DAO/Result_Details_DAO.inc";
	include_once "../Data/Table.inc";
	include_once "../Data/Table_Goals.inc";

	class Table_Service implements Service_Interface {

		private $JSON_Helper;

		private $team_DAO;

		private $league_DAO;

		private $result_DAO;

		private $details_DAO;

		private $match_DAO;

		private $player_DAO;

		private $teams_array;

		private $players_array;

		function Table_Service(){
			$this->JSON_Helper = new JSON_Data_Helper();
			$this->team_DAO = new Team_DAO();
			$this->league_DAO = new League_DAO();
			$this->result_DAO = new Result_DAO();
			$this->details_DAO = new Result_Details_DAO();
			$this->match_DAO = new Match_DAO();
			$this->player_DAO = new Player_DAO();
		}

		public function create($object) {
			
		}

		public function get_One($id) {
			//Implementar funcionalidad
		}

		public function get_All() {

		}

		public function get_Table_Goals_By_League($league) {
			$teams = $this->team_DAO->find_By_League($league);

			$this->players_array = array();

			foreach ($teams as $value) {
				$players = $this->player_DAO->find_By_Team($value->get_id());
				foreach ($players as $value) {
					$this->players_array[$value->get_id()] = new Table_Goals();
				}
			}

			$matches = $this->match_DAO->find_By_League($league);

			foreach($matches as $match) {
				$result_Entity = $this->result_DAO->find_By_Match($match->get_id());
				$result_Details = $this->details_DAO->find_By_Result($result_Entity->get_id());

				foreach ($result_Details as $value) {
					$table_Goals = new Table_Goals();

					$temp_Table_Goal = $this->players_array[$value->get_player()];

					$table_Goals->set_name($value->get_player());
					$table_Goals->set_goals($value->get_goals() + $temp_Table_Goal->get_goals());

					$this->players_array[$value->get_player()] = $table_Goals;
				}
			}

			$array_values = array();

			$array_result = array();

			foreach($this->players_array as $key => $value) {
				if($value->get_goals() > 0){
					$player = $this->player_DAO->find_By_Id($key);

					$team = $this->team_DAO->find_By_Id($player->get_team());

					$value->set_name($player->get_name());

					$value->set_photo(base64_encode($player->get_photo()));

					$value->set_team($team->get_name());

					array_push($array_values, $value);
				}
			}

			uasort($array_values, array($this, "sortPlayers"));

			foreach ($array_values as $value) {
				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		public function get_Table_By_League($league) {
			$teams = $this->team_DAO->find_By_League($league);

			$this->teams_array = array();

			foreach ($teams as $value) {
				$this->teams_array[$value->get_id()] = new Table();
			}

			$matches = $this->match_DAO->find_By_League($league);

			foreach($matches as $match) {
				$result_Entity = $this->result_DAO->find_By_Match($match->get_id());

				$local_Team = $match->get_local_Team();
				$foreign_Team = $match->get_foreign_Team();

				$local_Goals = $result_Entity->get_local_Goals();
				$foreign_Goals = $result_Entity->get_foreign_Goals();

				//SET LOCAL
				$this->process_Local_Team($local_Team, $foreign_Team, $local_Goals, $foreign_Goals);

				//SET FOREIGN
				$this->process_Foreign_Team($local_Team, $foreign_Team, $local_Goals, $foreign_Goals);

			}

			$array_values = array();

			$array_result = array();

			foreach($this->teams_array as $key => $value) {
				$team = $this->team_DAO->find_By_Id($key);

				$value->set_team(base64_encode($team->get_logo()));

				$value->set_team_Name($team->get_name());

				$value->set_points($this->generate_Points($value));

				array_push($array_values, $value);
			}

			uasort($array_values, array($this, "sortTeams"));

			foreach ($array_values as $value) {
				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		private function generate_Points($table) {
			return ($table->get_won() * 3) + $table->get_drawn();
		}

		private function sortTeams($a, $b) {
		    return ($a->get_points() > $b->get_points()) ? -1 : (($a->get_points() < $b->get_points()) ? 1 : 0);
		}

		private function sortPlayers($a, $b) {
		    return ($a->get_goals() > $b->get_goals()) ? -1 : (($a->get_goals() < $b->get_goals()) ? 1 : 0);
		}

		private function process_Local_Team($local_Team, $foreign_Team, $local_Goals, $foreign_Goals) {
			$table = new Table();
			$table->set_favor($local_Goals);
			$table->set_against($foreign_Goals);

			$table->set_team($local_Team);

			$result_Goals = $this->discover_Result($local_Goals, $foreign_Goals);

			if($result_Goals == 1) {
				$table->set_won(1);
			}elseif($result_Goals == -1) {
				$table->set_lost(1);
			}elseif($result_Goals == 0)
				$table->set_drawn(1);
			

			$this->update_Table_Entity($table, $local_Team);
		}

		private function process_Foreign_Team($local_Team, $foreign_Team, $local_Goals, $foreign_Goals) {
			$table = new Table();
			$table->set_favor($foreign_Goals);
			$table->set_against($local_Goals);

			$table->set_team($foreign_Team);

			$result_Goals = $this->discover_Result($foreign_Goals, $local_Goals);

			if($result_Goals == 1) {
				$table->set_won(1);
			}elseif($result_Goals == -1) {
				$table->set_lost(1);
			}else
				$table->set_drawn(1);

			$this->update_Table_Entity($table, $foreign_Team);
		}

		private function update_Table_Entity($table, $team) {
			$tempTable = $this->teams_array[$team];

			$table->set_favor($tempTable->get_favor() +  $table->get_favor());
			$table->set_against($tempTable->get_against() + $table->get_against());
			$table->set_won($tempTable->get_won() +  $table->get_won());
			$table->set_drawn($tempTable->get_drawn() +  $table->get_drawn());
			$table->set_lost($tempTable->get_lost() +  $table->get_lost());
			$table->set_games($tempTable->get_games() + 1);

			$this->teams_array[$team] = $table;
		}

		private function discover_Result($team, $team2) {
			if($team > $team2) {
				return 1;
			}elseif($team < $team2) {
				return -1;
			}
			return 0;
		}

		public function modify($entidad) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_By_Name($nombre) {
			// TODO: Implementar funcionalidad
		}

		private function generate_Fancy_Match($entity) {
			


			$foreign_Team = $this->team_DAO->find_By_Id($entity->get_foreign_Team());
			$court = $this->court_DAO->find_By_Id($entity->get_court());
			$referee = $this->referee_DAO->find_By_Id($entity->get_referee());

			$entity->set_league($league->get_name());
			$entity->set_local_Team($local_Team->get_name());
			$entity->set_foreign_Team($foreign_Team->get_name());
			$entity->set_court($court->get_name());
			$entity->set_referee($referee->get_name());

			$entity->set_local_Logo(base64_encode($local_Team->get_logo()));
			$entity->set_foreign_Logo(base64_encode($foreign_Team->get_logo()));

			return $entity;
		}

	}
?>