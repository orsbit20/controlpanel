<?php
	include_once "Service_Interface.inc";
	include_once "Helpers/JSON_Data_Helper.inc";
	include_once "../DAO/Team_DAO.inc";
	include_once "../DAO/League_DAO.inc";
	include_once "../DAO/Match_DAO.inc";
	include_once "../Data/Team.inc";

	class Team_Service implements Service_Interface {

		private $JSON_Helper;

		private $team_DAO;

		private $match_DAO;

		private $league_DAO;

		function Team_Service(){
			$this->JSON_Helper = new JSON_Data_Helper();
			$this->team_DAO = new Team_DAO();
			$this->league_DAO = new League_DAO();
			$this->match_DAO = new Match_DAO();
		}

		public function create($object) {
			if(!$this->validate_Photo($object->get_logo())) {
				return null;
			}else {
				$object->set_logo($this->convert_Photo_To_String($object->get_logo()));
				$team = $this->team_DAO->save($object);
				return $this->JSON_Helper->transform_Entity_To_JSON($team->serialize_JSON());
			}
		}

		public function get_One($id) {
			$team = $this->team_DAO->find_By_Id($id);

			$team->set_logo(base64_encode($team->get_logo()));

			return $this->JSON_Helper->transform_Entity_To_JSON($team->serialize_JSON());
		}

		public function get_All() {
			$result = $this->team_DAO->find_All();

			$array_result = array();

			foreach($result as $value) {
				$league = $this->league_DAO->find_By_Id($value->get_league());

				$value->set_league($league->get_name());

				$value->set_logo(base64_encode($value->get_logo()));

				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		public function get_All_By_League($league) {
			$result = $this->team_DAO->find_By_League($league);

			$array_result = array();

			foreach($result as $value) {
				$league = $this->league_DAO->find_By_Id($value->get_league());

				$value->set_league($league->get_name());

				$value->set_logo(base64_encode($value->get_logo()));

				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		public function get_Teams_By_Match($match) {
			$match = $this->match_DAO->find_By_Id($match);

			$array_result = array();

			$localTeam = $this->team_DAO->find_By_Id($match->get_local_Team());
			$foreignTeam = $this->team_DAO->find_By_Id($match->get_foreign_Team());

			$localTeam->set_logo(base64_encode($localTeam->get_logo()));
			$foreignTeam->set_logo(base64_encode($foreignTeam->get_logo()));

			array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($localTeam->serialize_JSON()));
			array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($foreignTeam->serialize_JSON()));
			
			return $array_result;
		}

		public function modify($entidad) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_By_Name($nombre) {
			// TODO: Implementar funcionalidad
		}

		private function validate_Photo($photo) {
			if($photo['error'] > 0) {
				return false;
			}else {
				$type_Allowed = array("image/jpg", "image/jpeg", "image/png");
				$size_limit = 16384;

				if(in_array($photo['type'], $type_Allowed) && $photo['size'] <= 1024 * $size_limit) {
					return true;
				}
			}
		}

		private function convert_Photo_To_String($photo) {
			$temp_image = $photo["tmp_name"];
			$type = $photo["type"];

			$fp = fopen($temp_image, 'r+b');
			$data = fread($fp, filesize($temp_image));
			fclose($fp);

			return mysql_escape_string($data);
		}

	}
?>