<?php
	include_once "Service_Interface.inc";
	include_once "Helpers/JSON_Data_Helper.inc";
	include_once "../DAO/Result_DAO.inc";
	include_once "../DAO/Result_Details_DAO.inc";
	include_once "../Data/Result.inc";

	class Result_Service implements Service_Interface {

		private $JSON_Helper;

		private $result_DAO;

		private $result_Details_DAO;

		function Result_Service(){
			$this->JSON_Helper = new JSON_Data_Helper();
			$this->result_DAO = new Result_DAO();
			$this->result_Details_DAO = new Result_Details_DAO();
		}

		public function create($result) {
			$result = $this->result_DAO->save($result);

			return $this->JSON_Helper->transform_Entity_To_JSON($resultComplete->serialize_JSON());
		}

		public function create_Results($result, $details) {
			$result = $this->result_DAO->save($result);
			$resultComplete = $this->result_DAO->find_By_All($result);

			foreach ($details as $value) {
				$value->set_result($resultComplete->get_id());
				$this->result_Details_DAO->save($value);
			}

			return $this->JSON_Helper->transform_Entity_To_JSON($resultComplete->serialize_JSON());
		}

		public function get_One($id) {
			//Implementar funcionalidad
		}

		public function get_All() {

		}

		public function modify($entidad) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_By_Name($nombre) {
			// TODO: Implementar funcionalidad
		}
	}
?>