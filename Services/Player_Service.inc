<?php
	include_once "Service_Interface.inc";
	include_once "Helpers/JSON_Data_Helper.inc";
	include_once "../DAO/Team_DAO.inc";
	include_once "../DAO/Player_DAO.inc";
	include_once "../DAO/League_DAO.inc";
	include_once "../Data/Player.inc";

	class Player_Service implements Service_Interface {

		private $JSON_Helper;

		private $team_DAO;

		private $player_DAO;

		private $league_DAO;

		function Player_Service(){
			$this->JSON_Helper = new JSON_Data_Helper();
			$this->team_DAO = new Team_DAO();
			$this->player_DAO = new Player_DAO();
			$this->league_DAO = new League_DAO();
		}

		public function create($object) {
			if(!$this->validate_Photo($object->get_photo())) {
				return null;
			}else {
				$object->set_photo($this->convert_Photo_To_String($object->get_photo()));
				$player = $this->player_DAO->save($object);
				return $this->JSON_Helper->transform_Entity_To_JSON($player->serialize_JSON());
			}
		}

		public function get_One($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_All() {
			$result = $this->player_DAO->find_All();

			$array_result = array();

			foreach($result as $value) {
				$team = $this->team_DAO->find_By_Id($value->get_team());

				$league = $this->league_DAO->find_By_Id($team->get_league());

				$value->set_team($team->get_name());

				$value->set_league_Name($league->get_name());

				$value->set_photo(base64_encode($value->get_photo()));

				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		public function get_Players_By_Team($team) {
			$result = $this->player_DAO->find_By_Team($team);

			$array_result = array();

			foreach($result as $value) {
				$value->set_photo(base64_encode($value->get_photo()));

				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		public function get_Players_By_League($league) {
			$result = $this->player_DAO->find_By_League($league);

			$array_result = array();

			foreach($result as $value) {
				$team = $this->team_DAO->find_By_Id($value->get_team());

				$league = $this->league_DAO->find_By_Id($team->get_league());

				$value->set_team($team->get_name());

				$value->set_league_Name($league->get_name());
				
				$value->set_photo(base64_encode($value->get_photo()));

				array_push($array_result, $this->JSON_Helper->transform_Entity_To_JSON($value->serialize_JSON()));
			}

			return $array_result;
		}

		public function modify($entidad) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function get_By_Name($nombre) {
			// TODO: Implementar funcionalidad
		}

		private function validate_Photo($photo) {
			if($photo['error'] > 0) {
				return false;
			}else {
				$type_Allowed = array("image/jpg", "image/jpeg", "image/png");
				$size_limit = 16384;
				if(in_array($photo['type'], $type_Allowed) && $photo['size'] <= 1024 * $size_limit) {
					return true;
				}
			}
		}

		private function convert_Photo_To_String($photo) {
			$temp_image = $photo["tmp_name"];
			$type = $photo["type"];

			$fp = fopen($temp_image, 'r+b');
			$data = fread($fp, filesize($temp_image));
			fclose($fp);

			return mysql_escape_string($data);
		}

	}
?>