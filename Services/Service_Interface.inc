<?php
	interface Service_Interface {

		public function create($JSON_Data);

		public function get_One($id);

		public function get_All();

		public function modify($entidad);

		public function delete($id);
	}
?>