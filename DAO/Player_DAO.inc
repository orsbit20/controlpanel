<?php
	include_once "DAO_Interface.inc";
	include_once "../Infrastructure/config.php";
	include_once "../Helpers/SQL_Query_Helper.inc";
	include_once "../Helpers/Config_Helper.inc";
	include_once "../Data/Player.inc";

	class Player_DAO implements DAO_Interface {

		private $sql_Helper;
		private $config_helper;

		function Player_DAO() {
			$this->sql_Helper = new SQL_Query_Helper();
			$this->config_helper = new Config_Helper();
		}

		public function save($player_Entity) {

			$column_value_Map = array(
				'ID_EQUIPO' => $player_Entity->get_team(),
				'NOMBRE' => $player_Entity->get_name(),
				'NUMERO' => $player_Entity->get_number(),
				'FOTO' => $player_Entity->get_photo(),
			);

			$playerTable = $this->config_helper->get_Table_Name("playerTable");

			$query = $this->sql_Helper->insert($playerTable, $column_value_Map);

			$result = mysql_query($query);

			if($result === false){
				return null;
			}

			return $player_Entity;
		}

		public function find_By_Id($id) {
			$value_Map = array(
				'ID' => $id,
			);

			$playerTable = $this->config_helper->get_Table_Name("playerTable");

			$query = $this->sql_Helper->select_All_From_Where_Active($playerTable, $value_Map);

			$result = mysql_query($query);

			$player = new Player();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$player = $this->transform_To_Entity($value);
			}	

			return $player;
		}

		public function find_All() {
			$playerTable = $this->config_helper->get_Table_Name("playerTable");

			$query = $this->sql_Helper->select_All_From_Active($playerTable);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function find_By_Team($team) {
			$value_Map = array(
				'ID_EQUIPO' => $team,
			);

			$playerTable = $this->config_helper->get_Table_Name("playerTable");

			$query = $this->sql_Helper->select_All_From_Where_Active($playerTable, $value_Map);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function find_By_League($league) {
			$playerTable = $this->config_helper->get_Table_Name("playerTable");
			$teamTable = $this->config_helper->get_Table_Name("teamTable");

			$value_Map = array(
				$teamTable.".".'ID_LIGA' => $league,
			);

			$get_Value_Array = array(
				$playerTable.".".'ID',
				$playerTable.".".'ID_EQUIPO',
				$playerTable.".".'NOMBRE',
				$playerTable.".".'NUMERO',
				$playerTable.".".'FOTO',
				$playerTable.".".'ACTIVO',
			);

			$query = $this->sql_Helper->select_All_From_Inner_Join_Where_Active($playerTable, $teamTable, $get_Value_Array, $value_Map);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function update($team_Entity) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function find_By_Name($team_Entity) {
			// TODO: Implementar funcionalidad
		}

		private function transform_To_Entity($result) {
			$player = new Player();

			$player->set_id($result["ID"]);
			$player->set_team($result["ID_EQUIPO"]);
			$player->set_name($result["NOMBRE"]);
			$player->set_number($result["NUMERO"]);
			$player->set_photo($result["FOTO"]);
			$player->set_active($result["ACTIVO"]);
			
			return $player;
		}
	}
?>