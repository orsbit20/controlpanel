<?php
	include_once "DAO_Interface.inc";
	include_once "../Infrastructure/config.php";
	include_once "../Helpers/SQL_Query_Helper.inc";
	include_once "../Helpers/Config_Helper.inc";
	include_once "../Data/Team.inc";

	class Team_DAO implements DAO_Interface {

		private $sql_Helper;
		private $config_helper;

		function Team_DAO() {
			$this->sql_Helper = new SQL_Query_Helper();
			$this->config_helper = new Config_Helper();
		}

		public function save($team_Entity) {

			$column_value_Map = array(
				'ID_LIGA' => $team_Entity->get_league(),
				'NOMBRE' => $team_Entity->get_name(),
				'DESCRIPCION' => $team_Entity->get_description(),
				'LOGO' => $team_Entity->get_logo(),
				'NOMBRE_CONTACTO' => $team_Entity->get_contact_Name(),
				'TELEFONO' => $team_Entity->get_phone(),
				'FECHA_INGRESO' => $team_Entity->get_registry_Date(),
			);

			$team_Table = $this->config_helper->get_Table_Name("teamTable");

			$query = $this->sql_Helper->insert($team_Table, $column_value_Map);

			$result = mysql_query($query);

			if($result === false){
				return null;
			}

			return $team_Entity;
		}

		public function find_By_Id($id) {
			$value_Map = array(
				'ID' => $id,
			);

			$teamTable = $this->config_helper->get_Table_Name("teamTable");

			$query = $this->sql_Helper->select_All_From_Where_Active($teamTable, $value_Map);

			$result = mysql_query($query);

			$team = new Team();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$team = $this->transform_To_Entity($value);
			}	

			return $team;
		}

		public function find_By_League($league) {
			$value_Map = array(
				'ID_LIGA' => $league,
			);

			$teamTable = $this->config_helper->get_Table_Name("teamTable");

			$query = $this->sql_Helper->select_All_From_Where_Active($teamTable, $value_Map);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function find_All() {
			$teamTable = $this->config_helper->get_Table_Name("teamTable");

			$query = $this->sql_Helper->select_All_From_Active($teamTable);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function update($team_Entity) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function find_By_Name($team_Entity) {
			// TODO: Implementar funcionalidad
		}

		private function transform_To_Entity($result) {
			$team = new Team();

			$team->set_id($result["ID"]);
			$team->set_league($result["ID_LIGA"]);
			$team->set_name($result["NOMBRE"]);
			$team->set_logo($result["LOGO"]);
			$team->set_description($result["DESCRIPCION"]);
			$team->set_contact_Name($result["NOMBRE_CONTACTO"]);
			$team->set_phone($result["TELEFONO"]);
			$team->set_registry_Date($result["FECHA_INGRESO"]);
			$team->set_active($result["ACTIVO"]);

			return $team;
		}
	}
?>