<?php
	include_once "DAO_Interface.inc";
	include_once "../Infrastructure/config.php";
	include_once "../Helpers/SQL_Query_Helper.inc";
	include_once "../Helpers/Config_Helper.inc";
	include_once "../Data/Court.inc";

	class Court_DAO implements DAO_Interface {

		private $sql_Helper;
		private $config_helper;

		function Court_DAO() {
			$this->sql_Helper = new SQL_Query_Helper();
			$this->config_helper = new Config_Helper();
		}

		public function save($court_Entity) {

			$column_value_Map = array(
				'ID_EMPRESA' => $court_Entity->get_company(),
				'NOMBRE' => $court_Entity->get_name(),
				'DESCRIPCION' => $court_Entity->get_description(),
				'LATITUD' => $court_Entity->get_latitude(),
				'LONGITUD' => $court_Entity->get_length(),
			);

			$court_Table = $this->config_helper->get_Table_Name("courtTable");

			$query = $this->sql_Helper->insert($court_Table, $column_value_Map);

			$result = mysql_query($query);

			if($result === false){
				return null;
			}

			return $court_Entity;
		}

		public function find_By_Id($id) {
			$value_Map = array(
				'ID' => $id,
			);

			$courtTable = $this->config_helper->get_Table_Name("courtTable");

			$query = $this->sql_Helper->select_All_From_Where_Active($courtTable, $value_Map);

			$result = mysql_query($query);

			$court = new Court();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$court = $this->transform_To_Entity($value);
			}	

			return $court;
		}

		public function find_All() {
			$courtTable = $this->config_helper->get_Table_Name("courtTable");

			$query = $this->sql_Helper->select_All_From_Active($courtTable);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function find_By_Company($company) {
			$value_Map = array(
				'ID_EMPRESA' => $company,
			);

			$courtTable = $this->config_helper->get_Table_Name("courtTable");

			$query = $this->sql_Helper->select_All_From_Where_Active($courtTable, $value_Map);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function update($user_Entity) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function find_By_Name($user_Entity) {
			// TODO: Implementar funcionalidad
		}

		private function transform_To_Entity($result) {
			$court = new Court();

			$court->set_id($result["ID"]);
			$court->set_name($result["NOMBRE"]);
			$court->set_company($result["ID_EMPRESA"]);
			$court->set_description($result["DESCRIPCION"]);
			$court->set_latitude($result["LATITUD"]);
			$court->set_length($result["LONGITUD"]);
			$court->set_active($result["ACTIVO"]);

			return $court;
		}
	}
?>