<?php
	interface DAO_Interface {

		public function save($entidad);

		public function find_By_Id($id);

		public function find_All();

		public function update($entidad);

		public function delete($id);
	}
?>