<?php
	include_once "DAO_Interface.inc";
	include_once "../Infrastructure/config.php";
	include_once "../Helpers/SQL_Query_Helper.inc";
	include_once "../Helpers/Config_Helper.inc";
	include_once "../Data/Company.inc";

	class Company_DAO implements DAO_Interface {

		private $sql_Helper;
		private $config_helper;

		function Company_DAO() {
			$this->sql_Helper = new SQL_Query_Helper();
			$this->config_helper = new Config_Helper();
		}

		public function save($user_Entity) {
			// TODO: Implementar funcionalidad
		}

		public function find_By_Id($id) {
			// TODO: Implementar funcionalidad
		}

		public function find_All() {
			$companyTable = $this->config_helper->get_Table_Name("companyTable");

			$query = $this->sql_Helper->select_All_From_Active($companyTable);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function update($user_Entity) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function find_By_Name($user_Entity) {
			// TODO: Implementar funcionalidad
		}

		private function transform_To_Entity($result) {
			$company = new Company();

			$company->set_id($result["ID"]);
			$company->set_name($result["NOMBRE"]);
			$company->set_address($result["UBICACION"]);
			$company->set_phone($result["TELEFONO"]);
			$company->set_web_Page($result["PAGINA_WEB"]);
			$company->set_rfc($result["RFC"]);

			return $company;
		}
	}
?>