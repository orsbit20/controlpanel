<?php
	include_once "DAO_Interface.inc";
	include_once "../Infrastructure/config.php";
	include_once "../Helpers/SQL_Query_Helper.inc";
	include_once "../Helpers/Config_Helper.inc";
	include_once "../Data/News.inc";

	class News_DAO implements DAO_Interface {

		private $sql_Helper;
		private $config_helper;

		function News_DAO() {
			$this->sql_Helper = new SQL_Query_Helper();
			$this->config_helper = new Config_Helper();
		}

		public function save($entity) {

			$column_value_Map = array(
				'TITULO' => $entity->get_title(),
				'CONTENIDO' => $entity->get_content(),
				'FOTO' => $entity->get_photo(),
				'FECHA_PUBLICACION' => $entity->get_publish_Date(),
			);

			$newsTable = $this->config_helper->get_Table_Name("newsTable");

			$query = $this->sql_Helper->insert($newsTable, $column_value_Map);

			$result = mysql_query($query);

			if($result === false){
				return null;
			}

			return $entity;
		}

		public function find_By_Id($id) {
			// TODO: Implementar funcionalidad
		}

		public function find_All() {
			$newsTable = $this->config_helper->get_Table_Name("newsTable");

			$query = $this->sql_Helper->select_All_From_Active($newsTable);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function find_By_Company($company) {
			$value_Map = array(
				'ID_EMPRESA' => $company,
			);

			$newsTable = $this->config_helper->get_Table_Name("newsTable");

			$query = $this->sql_Helper->select_All_From_Where_Active($newsTable, $value_Map);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function update($news_Entity) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function find_By_Name($news_Entity) {
			// TODO: Implementar funcionalidad
		}

		private function transform_To_Entity($result) {
			$news = new News();

			$news->set_id($result["ID"]);
			$news->set_title($result["TITULO"]);
			$news->set_content($result["CONTENIDO"]);
			$news->set_photo($result["FOTO"]);
			$news->set_publish_Date($result["FECHA_PUBLICACION"]);
			$news->set_active($result["ACTIVO"]);
			
			return $news;
		}
	}
?>