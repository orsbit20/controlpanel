<?php
	include_once "DAO_Interface.inc";
	include_once "../Infrastructure/config.php";
	include_once "../Helpers/SQL_Query_Helper.inc";
	include_once "../Helpers/Config_Helper.inc";
	include_once "../Data/Match.inc";

	class Match_DAO implements DAO_Interface {

		private $sql_Helper;
		private $config_helper;
		private $table;

		function Match_DAO() {
			$this->sql_Helper = new SQL_Query_Helper();
			$this->config_helper = new Config_Helper();
			$this->table = $this->config_helper->get_Table_Name("matchTable");
		}

		public function save($entity) {

			$column_value_Map = array(
				'NUMERO' => $entity->get_number(),
				'ID_LIGA' => $entity->get_league(),
				'ID_EQUIPO_LOCAL' => $entity->get_local_Team(),
				'ID_EQUIPO_VISITANTE' => $entity->get_foreign_Team(),
				'ID_CANCHA' => $entity->get_court(),
				'FECHA' => $entity->get_date(),
				'ID_ARBITRO' => $entity->get_referee(),
			);

			$query = $this->sql_Helper->insert($this->table, $column_value_Map);

			$result = mysql_query($query);

			if($result === false){
				return null;
			}

			return $entity;
		}

		public function find_By_Id($id) {
			$value_Map = array(
				'ID' => $id,
			);

			$query = $this->sql_Helper->select_All_From_Where_Active($this->table, $value_Map);

			$result = mysql_query($query);

			$match = new Match();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$match = $this->transform_To_Entity($value);
			}	

			return $match;
		}

		public function find_By_League($league) {
			$value_Map = array(
				'ID_LIGA' => $league,
			);

			$query = $this->sql_Helper->select_All_From_Where_Active($this->table, $value_Map);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function find_All() {
			$query = $this->sql_Helper->select_All_From_Active($this->table);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function update($team_Entity) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function find_By_Name($team_Entity) {
			// TODO: Implementar funcionalidad
		}

		private function transform_To_Entity($result) {
			$match = new Match();

			$match->set_id($result["ID"]);
			$match->set_league($result["ID_LIGA"]);
			$match->set_number($result["NUMERO"]);
			$match->set_local_Team($result["ID_EQUIPO_LOCAL"]);
			$match->set_foreign_Team($result["ID_EQUIPO_VISITANTE"]);
			$match->set_court($result["ID_CANCHA"]);
			$match->set_date($result["FECHA"]);
			$match->set_referee($result["ID_ARBITRO"]);
			$match->set_active($result["ACTIVO"]);

			return $match;
		}
	}
?>