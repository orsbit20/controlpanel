<?php
	include_once "DAO_Interface.inc";
	include_once "../Infrastructure/config.php";
	include_once "../Helpers/SQL_Query_Helper.inc";
	include_once "../Helpers/Config_Helper.inc";
	include_once "../Data/Referee.inc";

	class Referee_DAO implements DAO_Interface {

		private $sql_Helper;
		private $config_helper;

		function Referee_DAO() {
			$this->sql_Helper = new SQL_Query_Helper();
			$this->config_helper = new Config_Helper();
		}

		public function save($referee_Entity) {

			$column_value_Map = array(
				'NOMBRE' => $referee_Entity->get_name(),
				'UBICACION' => $referee_Entity->get_address(),
				'TELEFONO' => $referee_Entity->get_phone(),
				'PAGINA_WEB' => $referee_Entity->get_web_Page(),
				'RFC' => $referee_Entity->get_rfc(),
			);

			$referee_Table = $this->config_helper->get_Table_Name("refereeTable");

			$query = $this->sql_Helper->insert($referee_Table, $column_value_Map);

			$result = mysql_query($query);

			if($result === false){
				return null;
			}

			return $referee_Entity;
		}

		public function find_By_Id($id) {
			$value_Map = array(
				'ID' => $id,
			);

			$refereeTable = $this->config_helper->get_Table_Name("refereeTable");

			$query = $this->sql_Helper->select_All_From_Where_Active($refereeTable, $value_Map);

			$result = mysql_query($query);

			$referee = new Referee();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$referee = $this->transform_To_Entity($value);
			}	

			return $referee;
		}

		public function find_All() {
			$refereeTable = $this->config_helper->get_Table_Name("refereeTable");

			$query = $this->sql_Helper->select_All_From_Active($refereeTable);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function update($user_Entity) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function find_By_Name($user_Entity) {
			// TODO: Implementar funcionalidad
		}

		private function transform_To_Entity($result) {
			$referee = new Referee();

			$referee->set_id($result["ID"]);
			$referee->set_name($result["NOMBRE"]);
			$referee->set_address($result["UBICACION"]);
			$referee->set_phone($result["TELEFONO"]);
			$referee->set_web_Page($result["PAGINA_WEB"]);
			$referee->set_rfc($result["RFC"]);
			$referee->set_active($result["ACTIVO"]);

			return $referee;
		}
	}
?>