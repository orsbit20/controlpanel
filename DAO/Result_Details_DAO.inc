<?php
	include_once "DAO_Interface.inc";
	include_once "../Infrastructure/config.php";
	include_once "../Helpers/SQL_Query_Helper.inc";
	include_once "../Helpers/Config_Helper.inc";
	include_once "../Data/Result_Details.inc";

	class Result_Details_DAO implements DAO_Interface {

		private $sql_Helper;
		private $config_helper;
		private $table;

		function Result_Details_DAO() {
			$this->sql_Helper = new SQL_Query_Helper();
			$this->config_helper = new Config_Helper();
			$this->table = $this->config_helper->get_Table_Name("resultDetailsTable");
		}

		public function save($entity) {

			$column_value_Map = array(
				'ID_RESULTADO' => $entity->get_result(),
				'ID_JUGADOR' => $entity->get_player(),
				'GOLES' => $entity->get_goals(),
				'TARJETA_AMARILLA' => $entity->get_yellow_Cards(),
				'TARJETA_ROJA' => $entity->get_red_Cards(),
				'EXTRAS' => $entity->get_extras(),
			);

			$query = $this->sql_Helper->insert($this->table, $column_value_Map);

			$result = mysql_query($query);

			if($result === false){
				return null;
			}

			return $entity;
		}

		public function find_By_Id($id) {
			$value_Map = array(
				'ID' => $id,
			);

			$query = $this->sql_Helper->select_All_From_Where_Active($this->table, $value_Map);

			$result = mysql_query($query);

			$result_Details = new Result_Details();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$result_Details = $this->transform_To_Entity($value);
			}	

			return $result_Details;
		}

		public function find_By_Result($id) {
			$value_Map = array(
				'ID_RESULTADO' => $id,
			);

			$query = $this->sql_Helper->select_All_From_Where_Active($this->table, $value_Map);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function find_All() {
			$query = $this->sql_Helper->select_All_From_Active($this->table);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function update($team_Entity) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function find_By_Name($team_Entity) {
			// TODO: Implementar funcionalidad
		}

		private function transform_To_Entity($result) {
			$result_Detail = new Result_Details();

			$result_Detail->set_id($result["ID"]);
			$result_Detail->set_result($result["ID_RESULTADO"]);
			$result_Detail->set_player($result["ID_JUGADOR"]);
			$result_Detail->set_goals($result["GOLES"]);
			$result_Detail->set_red_Cards($result["TARJETA_ROJA"]);
			$result_Detail->set_yellow_Cards($result["TARJETA_AMARILLA"]);
			$result_Detail->set_extras($result["EXTRAS"]);
			$result_Detail->set_active($result["ACTIVO"]);

			return $result_Detail;
		}
	}
?>