<?php
	include_once "DAO_Interface.inc";
	include_once "../Infrastructure/config.php";
	include_once "../Helpers/SQL_Query_Helper.inc";
	include_once "../Helpers/Config_Helper.inc";
	include_once "../Data/Result.inc";

	class Result_DAO implements DAO_Interface {

		private $sql_Helper;
		private $config_helper;
		private $table;

		function Result_DAO() {
			$this->sql_Helper = new SQL_Query_Helper();
			$this->config_helper = new Config_Helper();
			$this->table = $this->config_helper->get_Table_Name("resultTable");
		}

		public function save($entity) {

			$column_value_Map = array(
				'ID_JORNADA' => $entity->get_match(),
				'GOLES_EQUIPO_LOCAL' => $entity->get_local_Goals(),
				'GOLES_EQUIPO_VISITANTE' => $entity->get_foreign_Goals(),
				'EXTRAS' => $entity->get_extras(),
			);

			$query = $this->sql_Helper->insert($this->table, $column_value_Map);

			$result = mysql_query($query);

			if($result === false){
				return null;
			}

			return $entity;
		}

		public function find_By_Id($id) {
			$value_Map = array(
				'ID' => $id,
			);

			$query = $this->sql_Helper->select_All_From_Where_Active($this->table, $value_Map);

			$result = mysql_query($query);

			$resultEntity = new Result();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$resultEntity = $this->transform_To_Entity($value);
			}	

			return $resultEntity;
		}

		public function find_By_Match($id) {
			$value_Map = array(
				'ID_JORNADA' => $id,
			);

			$query = $this->sql_Helper->select_All_From_Where_Active($this->table, $value_Map);

			$result = mysql_query($query);

			$resultEntity = new Result();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$resultEntity = $this->transform_To_Entity($value);
			}	

			return $resultEntity;
		}

		public function find_By_All($resultEntity) {
			$value_Map = array(
				'ID_JORNADA' => $resultEntity->get_match(),
				'GOLES_EQUIPO_LOCAL' => $resultEntity->get_local_Goals(),
				'GOLES_EQUIPO_VISITANTE' => $resultEntity->get_foreign_Goals(),
				'EXTRAS' => $resultEntity->get_extras(),
			);

			$query = $this->sql_Helper->select_All_From_Where_Active($this->table, $value_Map);

			$resultFromDB = mysql_query($query);

			$resultEntity = new Result();

			while ($value = mysql_fetch_array($resultFromDB, MYSQL_ASSOC)) {
				$resultEntity = $this->transform_To_Entity($value);
			}	

			return $resultEntity;
		}

		public function find_All() {
			$query = $this->sql_Helper->select_All_From_Active($this->table);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function update($team_Entity) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function find_By_Name($team_Entity) {
			// TODO: Implementar funcionalidad
		}

		private function transform_To_Entity($result) {
			$result_Entity = new Result();

			$result_Entity->set_id($result["ID"]);
			$result_Entity->set_match($result["ID_JORNADA"]);
			$result_Entity->set_local_Goals($result["GOLES_EQUIPO_LOCAL"]);
			$result_Entity->set_foreign_Goals($result["GOLES_EQUIPO_VISITANTE"]);
			$result_Entity->set_extras($result["EXTRAS"]);
			$result_Entity->set_active($result["ACTIVO"]);

			return $result_Entity;
		}
	}
?>