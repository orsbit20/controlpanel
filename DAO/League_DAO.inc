<?php
	include_once "DAO_Interface.inc";
	include_once "../Infrastructure/config.php";
	include_once "../Helpers/SQL_Query_Helper.inc";
	include_once "../Helpers/Config_Helper.inc";
	include_once "../Data/League.inc";

	class League_DAO implements DAO_Interface {

		private $sql_Helper;
		private $config_helper;

		function League_DAO() {
			$this->sql_Helper = new SQL_Query_Helper();
			$this->config_helper = new Config_Helper();
		}

		public function save($league_Entity) {

			$column_value_Map = array(
				'ID_EMPRESA' => $league_Entity->get_company(),
				'NOMBRE' => $league_Entity->get_name(),
				'DESCRIPCION' => $league_Entity->get_description(),
				'PRECIOS' => $league_Entity->get_prices(),
				'FECHAS' => $league_Entity->get_dates(),
				'DIA' => $league_Entity->get_day(),
			);

			$leagueTable = $this->config_helper->get_Table_Name("leagueTable");

			$query = $this->sql_Helper->insert($leagueTable, $column_value_Map);

			$result = mysql_query($query);

			if($result === false){
				return null;
			}

			return $league_Entity;
		}

		public function find_By_Id($id) {

			$value_Map = array(
				'ID' => $id,
			);

			$leagueTable = $this->config_helper->get_Table_Name("leagueTable");

			$query = $this->sql_Helper->select_All_From_Where_Active($leagueTable, $value_Map);

			$result = mysql_query($query);

			$league = new League();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$league = $this->transform_To_Entity($value);
			}	

			return $league;
			
		}

		public function find_All() {
			$leagueTable = $this->config_helper->get_Table_Name("leagueTable");

			$query = $this->sql_Helper->select_All_From_Active($leagueTable);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function find_By_Company($company) {
			$value_Map = array(
				'ID_EMPRESA' => $company,
			);

			$leagueTable = $this->config_helper->get_Table_Name("leagueTable");

			$query = $this->sql_Helper->select_All_From_Where_Active($leagueTable, $value_Map);

			$result = mysql_query($query);

			$result_Array = array();

			while ($value = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($result_Array, $this->transform_To_Entity($value));
			}	

			return $result_Array;
		}

		public function update($league_Entity) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function find_By_Name($league_Entity) {
			// TODO: Implementar funcionalidad
		}

		private function transform_To_Entity($result) {
			$league = new League();

			$league->set_id($result["ID"]);
			$league->set_company($result["ID_EMPRESA"]);
			$league->set_name($result["NOMBRE"]);
			$league->set_description($result["DESCRIPCION"]);
			$league->set_prices($result["PRECIOS"]);
			$league->set_dates($result["FECHAS"]);
			$league->set_day($result["DIA"]);
			$league->set_active($result["ACTIVO"]);

			return $league;
		}
	}
?>