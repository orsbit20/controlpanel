<?php
	include_once "DAO_Interface.inc";
	include_once "../Infrastructure/config.php";
	include_once "../Helpers/SQL_Query_Helper.inc";
	include_once "../Helpers/Config_Helper.inc";
	include_once "../Data/User_Panel.inc";

	class User_Panel_DAO implements DAO_Interface {

		private $sql_Helper;
		private $config_helper;

		function User_Panel_DAO() {
			$this->sql_Helper = new SQL_Query_Helper();
			$this->config_helper = new Config_Helper();
		}

		public function save($user_Entity) {

			$column_value_Map = array(
				'USUARIO' => $user_Entity->get_user(),
				'PASS' => $user_Entity->get_pass(),
				'TIPO' => $user_Entity->get_type(),
				'ID_EMPRESA' => $user_Entity->get_company(),
			);

			$userTable = $this->config_helper->get_Table_Name("userTable");

			$query = $this->sql_Helper->insert($userTable, $column_value_Map);

			$result = mysql_query($query);

			if($result === false){
				return null;
			}

			return $user_Entity;
		}

		public function find_By_Id($id) {
			// TODO: Implementar funcionalidad
		}

		public function find_All() {
			// TODO: Implementar funcionalidad
		}

		public function update($user_Entity) {
			// TODO: Implementar funcionalidad
		}

		public function delete($id) {
			// TODO: Implementar funcionalidad
		}

		public function find_By_Name($user_Entity) {
			// TODO: Implementar funcionalidad
		}

		public function find_By_User_And_Pass($user, $pass) {

			$where_Rules_Map = array(
				'USUARIO' => $user,
				'PASS' => $pass,
			);

			$userTable = $this->config_helper->get_Table_Name("userTable");

			$query = $this->sql_Helper->select_All_From_Where_Active($userTable, $where_Rules_Map);

			$result = mysql_query($query);

			if($result == false) { 

				return null;

			}

			$user = $this->transform_To_Entity($result);

			return $user;
		}

		private function transform_To_Entity($result) {
			$user = new User_Panel();

			if ($result === false || mysql_num_rows($result) < 1) {
				return $user;
			}

			$user->set_id(mysql_result($result, 0, "ID"));
			$user->set_user(mysql_result($result, 0, "USUARIO"));
			$user->set_pass(mysql_result($result, 0, "PASS"));
			$user->set_type(mysql_result($result, 0, "TIPO"));
			$user->set_company(mysql_result($result, 0, "ID_EMPRESA"));

			return $user;
		}
	}
?>