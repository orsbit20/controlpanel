<?php

	include_once "JSON_Serializable.inc";
	
	class Entity extends JSON_Serializable{

		private $id;
		private $active;

		public function set_id($id) {
			$this->id = $id;
		}

		public function get_id() {
			return $this->id;
		}

		public function set_active($active) {
			$this->active = $active;
		}

		public function get_active() {
			return $this->active;
		}
	}

?>