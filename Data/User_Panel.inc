<?php
	
	include_once "User.inc";

	class User_Panel extends User {

		private $company;
		private $type;

		public function set_company($company) {
			$this->company = $company;
		}

		public function get_company() {
			return $this->company;
		}

		public function set_type($type) {
			$this->type = $type;
		}

		public function get_type() {
			return $this->type;
		}
	}

?>