<?php
	
	include_once "Entity.inc";

	class Court extends Entity {

		private $company;
		private $name;
		private $description;
		private $length;
		private $latitude;

		public function set_company($company) {
			$this->company = $company;
		}

		public function get_company() {
			return $this->company;
		}

		public function set_name($name) {
			$this->name = $name;
		}

		public function get_name() {
			return $this->name;
		}

		public function set_description($description) {
			$this->description = $description;
		}

		public function get_description() {
			return $this->description;
		}  

		public function set_length($length) {
			$this->length = $length;
		}

		public function get_length() {
			return $this->length;
		}  

		public function set_latitude($latitude) {
			$this->latitude = $latitude;
		}

		public function get_latitude() {
			return $this->latitude;
		}  
	}

?>