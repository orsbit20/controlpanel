<?php
	
	include_once "Entity.inc";

	class News extends Entity {

		private $title;
		private $content;
		private $publish_Date;
		private $photo;

		public function set_title($title) {
			$this->title = $title;
		}

		public function get_title() {
			return $this->title;
		}

		public function set_content($content) {
			$this->content = $content;
		}

		public function get_content() {
			return $this->content;
		}

		public function set_publish_Date($publish_Date) {
			$this->publish_Date = $publish_Date;
		}

		public function get_publish_Date() {
			return $this->publish_Date;
		}  

		public function set_photo($photo) {
			$this->photo = $photo;
		}

		public function get_photo() {
			return $this->photo;
		}  
	}

?>