<?php
	
	include_once "Entity.inc";

	class Referee extends Entity {

		private $name;
		private $address;
		private $phone;
		private $web_Page;
		private $rfc;

		public function set_name($name) {
			$this->name = $name;
		}

		public function get_name() {
			return $this->name;
		}

		public function set_address($address) {
			$this->address = $address;
		}

		public function get_address() {
			return $this->address;
		}

		public function set_phone($phone) {
			$this->phone = $phone;
		}

		public function get_phone() {
			return $this->phone;
		}  

		public function set_web_Page($web_Page) {
			$this->web_Page = $web_Page;
		}

		public function get_web_Page() {
			return $this->web_Page;
		}  

		public function set_rfc($rfc) {
			$this->rfc = $rfc;
		}

		public function get_rfc() {
			return $this->rfc;
		}  
	}

?>