<?php
	
	include_once "Entity.inc";

	class Result_Details extends Entity {

		private $result;
		private $player;
		private $goals;
		private $red_Cards;
		private $yellow_Cards;
		private $extras;

		public function set_result($result) {
			$this->result = $result;
		}

		public function get_result() {
			return $this->result;
		}

		public function set_player($player) {
			$this->player = $player;
		}

		public function get_player() {
			return $this->player;
		}

		public function set_goals($goals) {
			$this->goals = $goals;
		}

		public function get_goals() {
			return $this->goals;
		}  

		public function set_red_Cards($red_Cards) {
			$this->red_Cards = $red_Cards;
		}

		public function get_red_Cards() {
			return $this->red_Cards;
		}  

		public function set_yellow_Cards($yellow_Cards) {
			$this->yellow_Cards = $yellow_Cards;
		}

		public function get_yellow_Cards() {
			return $this->yellow_Cards;
		}  

		public function set_extras($extras) {
			$this->extras = $extras;
		}

		public function get_extras() {
			return $this->extras;
		}  
	}

?>