<?php
	
	include_once "Entity.inc";

	class Team extends Entity {

		private $league;
		private $name;
		private $logo;
		private $description;
		private $contact_Name;
		private $phone;
		private $registry_Date;

		public function set_league($league) {
			$this->league = $league;
		}

		public function get_league() {
			return $this->league;
		}

		public function set_name($name) {
			$this->name = $name;
		}

		public function get_name() {
			return $this->name;
		}

		public function set_logo($logo) {
			$this->logo = $logo;
		}

		public function get_logo() {
			return $this->logo;
		}  

		public function set_description($description) {
			$this->description = $description;
		}

		public function get_description() {
			return $this->description;
		}  

		public function set_contact_Name($contact_Name) {
			$this->contact_Name = $contact_Name;
		}

		public function get_contact_Name() {
			return $this->contact_Name;
		}  

		public function set_phone($phone) {
			$this->phone = $phone;
		}

		public function get_phone() {
			return $this->phone;
		}  

		public function set_registry_Date($registry_Date) {
			$this->registry_Date = $registry_Date;
		}

		public function get_registry_Date() {
			return $this->registry_Date;
		}  
	}

?>