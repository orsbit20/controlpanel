<?php
	
	include_once "Entity.inc";

	class Match extends Entity {

		private $number;
		private $league;
		private $local_Team;
		private $local_Logo;
		private $local_Goals;
		private $foreign_Team;
		private $foreign_Logo;
		private $foreign_Goals;
		private $court;
		private $date;
		private $referee;

		public function set_number($number) {
			$this->number = $number;
		}

		public function get_number() {
			return $this->number;
		}

		public function set_league($league) {
			$this->league = $league;
		}

		public function get_league() {
			return $this->league;
		}

		public function set_local_Team($local_Team) {
			$this->local_Team = $local_Team;
		}

		public function get_local_Team() {
			return $this->local_Team;
		}  

		public function set_local_Logo($local_Logo) {
			$this->local_Logo = $local_Logo;
		}

		public function get_local_Logo() {
			return $this->local_Logo;
		} 

		public function set_local_Goals($local_Goals) {
			$this->local_Goals = $local_Goals;
		}

		public function get_local_Goals() {
			return $this->local_Goals;
		}  

		public function set_foreign_Team($foreign_Team) {
			$this->foreign_Team = $foreign_Team;
		}

		public function get_foreign_Team() {
			return $this->foreign_Team;
		}  

		public function set_foreign_Logo($foreign_Logo) {
			$this->foreign_Logo = $foreign_Logo;
		}

		public function get_foreign_Logo() {
			return $this->foreign_Logo;
		} 

		public function set_foreign_Goals($foreign_Goals) {
			$this->foreign_Goals = $foreign_Goals;
		}

		public function get_foreign_Goals() {
			return $this->foreign_Goals;
		}  

		public function set_court($court) {
			$this->court = $court;
		}

		public function get_court() {
			return $this->court;
		}  

		public function set_date($date) {
			$this->date = $date;
		}

		public function get_date() {
			return $this->date;
		}  

		public function set_referee($referee) {
			$this->referee = $referee;
		}

		public function get_referee() {
			return $this->referee;
		}  
	}

?>