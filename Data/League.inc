<?php
	
	include_once "Entity.inc";

	class League extends Entity {

		private $company;
		private $name;
		private $description;
		private $prices;
		private $dates;
		private $day;

		public function set_company($company) {
			$this->company = $company;
		}

		public function get_company() {
			return $this->company;
		}

		public function set_name($name) {
			$this->name = $name;
		}

		public function get_name() {
			return $this->name;
		}

		public function set_description($description) {
			$this->description = $description;
		}

		public function get_description() {
			return $this->description;
		}  

		public function set_prices($prices) {
			$this->prices = $prices;
		}

		public function get_prices() {
			return $this->prices;
		}  

		public function set_dates($dates) {
			$this->dates = $dates;
		}

		public function get_dates() {
			return $this->dates;
		}  

		public function set_day($day) {
			$this->day = $day;
		}

		public function get_day() {
			return $this->day;
		}  
	}

?>