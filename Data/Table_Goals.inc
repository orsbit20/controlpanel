<?php
	
	include_once "Entity.inc";

	class Table_Goals extends Entity {

		private $photo;
		private $name;
		private $team;
		private $goals;

		public function set_photo($photo) {
			$this->photo = $photo;
		}

		public function get_photo() {
			return $this->photo;
		}

		public function set_name($name) {
			$this->name = $name;
		}

		public function get_name() {
			return $this->name;
		}

		public function set_team($team) {
			$this->team = $team;
		}

		public function get_team() {
			return $this->team;
		}

		public function set_goals($goals) {
			$this->goals = $goals;
		}

		public function get_goals() {
			return $this->goals;
		}
	}

?>