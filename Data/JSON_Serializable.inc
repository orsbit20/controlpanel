<?php

    abstract class JSON_Serializable {

        public function serialize_JSON() {
            $getter_names = get_class_methods(get_class($this));
            $gettable_attributes = array();
            foreach ($getter_names as $key => $value) {
                if(substr($value, 0, 4) === 'get_') {
                    $gettable_attributes[substr($value, 4, strlen($value))] = $this->$value();
                }
            }
            return $gettable_attributes;
        }

    }

?>