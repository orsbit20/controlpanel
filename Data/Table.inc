<?php
	
	include_once "Entity.inc";

	class Table extends Entity {

		private $league;
		private $team;
		private $team_Name;
		private $points;
		private $games;
		private $won;
		private $drawn;
		private $lost;
		private $favor;
		private $against;

		public function set_league($league) {
			$this->league = $league;
		}

		public function get_league() {
			return $this->league;
		}

		public function set_team($team) {
			$this->team = $team;
		}

		public function get_team() {
			return $this->team;
		}

		public function set_team_Name($team_Name) {
			$this->team_Name = $team_Name;
		}

		public function get_team_Name() {
			return $this->team_Name;
		}

		public function set_points($points) {
			$this->points = $points;
		}

		public function get_points() {
			return $this->points;
		}

		public function set_games($games) {
			$this->games = $games;
		}

		public function get_games() {
			return $this->games;
		}

		public function set_won($won) {
			$this->won = $won;
		}

		public function get_won() {
			return $this->won;
		}  

		public function set_drawn($drawn) {
			$this->drawn = $drawn;
		}

		public function get_drawn() {
			return $this->drawn;
		}  

		public function set_lost($lost) {
			$this->lost = $lost;
		}

		public function get_lost() {
			return $this->lost;
		}  

		public function set_favor($favor) {
			$this->favor = $favor;
		}

		public function get_favor() {
			return $this->favor;
		}  

		public function set_against($against) {
			$this->against = $against;
		}

		public function get_against() {
			return $this->against;
		}  
	}

?>