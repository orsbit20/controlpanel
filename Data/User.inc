<?php
	
	include_once "Entity.inc";

	class User extends Entity {

		private $user;
		private $pass;

		public function set_user($user) {
			$this->user = $user;
		}

		public function get_user() {
			return $this->user;
		}

		public function set_pass($pass) {
			$this->pass = $pass;
		}

		public function get_pass() {
			return $this->pass;
		}
	}

?>