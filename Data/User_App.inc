<?php
	
	include_once "User.inc";

	class User_App extends User {

		private $team;

		public function set_team($team) {
			$this->team = $team;
		}

		public function get_team() {
			return $this->team;
		}
	}

?>