<?php
	
	include_once "Entity.inc";

	class Player extends Entity {

		private $team;
		private $name;
		private $number;
		private $photo;
		private $league_Name;

		public function set_team($team) {
			$this->team = $team;
		}

		public function get_team() {
			return $this->team;
		}

		public function set_name($name) {
			$this->name = $name;
		}

		public function get_name() {
			return $this->name;
		}

		public function set_number($number) {
			$this->number = $number;
		}

		public function get_number() {
			return $this->number;
		}  

		public function set_photo($photo) {
			$this->photo = $photo;
		}

		public function get_photo() {
			return $this->photo;
		}  

		public function set_league_Name($league_Name) {
			$this->league_Name = $league_Name;
		}

		public function get_league_Name() {
			return $this->league_Name;
		}  
	}

?>