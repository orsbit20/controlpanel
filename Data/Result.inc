<?php
	
	include_once "Entity.inc";

	class Result extends Entity {

		private $match;
		private $local_Goals;
		private $foreign_Goals;
		private $extras;

		public function set_match($match) {
			$this->match = $match;
		}

		public function get_match() {
			return $this->match;
		}

		public function set_local_Goals($local_Goals) {
			$this->local_Goals = $local_Goals;
		}

		public function get_local_Goals() {
			return $this->local_Goals;
		}

		public function set_foreign_Goals($foreign_Goals) {
			$this->foreign_Goals = $foreign_Goals;
		}

		public function get_foreign_Goals() {
			return $this->foreign_Goals;
		}  

		public function set_extras($extras) {
			$this->extras = $extras;
		}

		public function get_extras() {
			return $this->extras;
		}  
	}

?>