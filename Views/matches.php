<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Home</title>
    <!-- Bootstrap Styles-->
    <link href="css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="css/font-awesome.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.html">FutGol Home</a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment
                                    <span class="pull-right text-muted small">4 min</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 min</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small">4 min</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">4 min</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 min</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="../Endpoints/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                    <li>
                        <a href="home.html"><i class="fa fa-dashboard"></i> Principal</a>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-user"></i> Altas<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            
                            <li>
                                <a href="#arbitros">Arbitros<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="referee-create.html">Nuevo</a>
                                    </li>
                                    <li>
                                        <a href="referee-view.html">Ver</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#canchas">Canchas<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="court-create.html">Nuevo</a>
                                    </li>
                                    <li>
                                        <a href="court-view.html">Ver</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#">Equipos<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="team-create.html">Nuevo</a>
                                    </li>
                                    <li>
                                        <a href="team-view.html">Ver</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#">Jugadores<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="player-create.html">Nuevo</a>
                                    </li>
                                    <li>
                                        <a href="player-view.html">Ver</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#">Ligas<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="league-create.html">Nuevo</a>
                                    </li>
                                    <li>
                                        <a href="league-view.html">Ver</a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </li>
                    
                    <li>
                        <a href="news.php"><i class="fa fa-file-text"></i> Noticias</a>
                    </li>

                    <li>
                        <a class="active-menu" href="matches.php"><i class="fa fa-calendar"></i> Jornadas</a>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-trophy"></i> Estadisticas</a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="results.php">Resultados<span class="fa arrow"></span></a>
                            <li>
                            <li>
                                <a href="league-table.php">Tabla de Posiciones<span class="fa arrow"></span></a>
                            <li>
                            <li>
                                <a href="player-goals.php">Tabla de Goleo<span class="fa arrow"></span></a>
                            <li>
                        </ul>
                    </li>
                </ul>
            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Jornadas <small>Armar Jornada</small>
                        </h1>
                        <?php

                            extract ($_REQUEST);

                            if(isset($_GET['success']) ) {
                                if($_GET['success'] == true) {
                        ?>
                                    <div class="alert alert-success">
                                        <strong>¡Exito!</strong> ¡Tu has escrito una noticia nueva!
                                    </div>
                        <?php

                                } else {
                        ?>
                                    <div class="alert alert-danger">
                                        <strong>¡Error!</strong> ¡Ocurrio un error!
                                    </div>
                        <?php

                                }
                            }

                        ?>
                        <div class="alert alert-warning" id="messageWarningLeague" hidden="true">
                            <strong>Atención!</strong> No hay ninguna liga creada, crea una liga primero.<a href="league-create.html"> Crea Liga</a>
                        </div>
                        <div class="alert alert-warning" id="messageWarningTeam" hidden="true">
                            <strong>Atención!</strong> No hay ningun equipo ligado a esta liga, liga un equipo primero.<a href="team-create.html"> Liga Equipo</a>
                        </div>
                        <div class="alert alert-warning" id="messageWarningOneTeam" hidden="true">
                            <strong>Atención!</strong> Solo hay un equipo en esta liga, añade otro equipo.<a href="team-create.html"> Liga Equipo</a>
                        </div>
                        <div class="alert alert-warning" id="messageWarningReferee" hidden="true">
                            <strong>Atención!</strong> No hay ningun arbitro creado, crea un arbitro primero.<a href="referee-create.html"> Crea Arbitro</a>
                        </div>
                        <div class="alert alert-warning" id="messageWarningCourt" hidden="true">
                            <strong>Atención!</strong> No hay ninguna cancha creada, crea una cancha primero.<a href="court-create.html"> Crea Cancha</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <form role="form" action="../Endpoints/Create_Match_Endpoint.php" method="post">

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Ligas</label>
                                    <select class="form-control" id="selectLeague" name="league">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Equipo Local</label>
                                    <select class="form-control" id="selectLocal" name="local_Team">
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Equipo Visitante</label>
                                    <select class="form-control" id="selectForeign" name="foreign_Team">
                                    </select>
                                </div>
                            </div>

                        </div> 

                         <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Canchas</label>
                                    <select class="form-control" id="selectCourt" name="court">
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Arbitros</label>
                                    <select class="form-control" id="selectReferee" name="referee">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Fecha</label>
                                    <input type="datetime-local" class="form-control" name="date" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Numero</label>
                                    <input class="form-control" placeholder="Numero Jornada" name="number" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <button id="createButtonId" type="submit" class="btn btn-default">Crear</button>
                            </div>
                        </div>  
                    </form>
                
                </div>
                <footer><p>Todos los derechos reservados. Orsbit 2015</a></p></footer>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- JS Scripts-->
    <!-- jQuery Js -->
    <script src="js/jquery-1.10.2.js"></script>
    <!-- Bootstrap Js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Metis Menu Js -->
    <script src="js/jquery.metisMenu.js"></script>
    <!-- Custom Js -->
    <script src="js/custom.js"></script>
    <!-- Function Page JS -->
    <script src="js/LocalFunctions/matches.js"></script>

    <script>
        $(document).ready(function () {
            loadReferees("messageWarningReferee", "selectReferee", "createButtonId");
            loadCourts("messageWarningCourt", "selectCourt", "createButtonId");
            loadLeagues("messageWarning", "messageWarningTeam", "messageWarningOneTeam", "selectLeague", "selectLocal", "selectForeign", "createButtonId");

            $("#selectLocal").on("change", function() {
                var element = this;
                noDuplicateTeams(element);
            });

            $("#selectForeign").on("change", function() {
                var element = this;
                noDuplicateTeams(element);
            });
        });
    </script>


</body>

</html>