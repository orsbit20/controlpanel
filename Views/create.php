<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Create User</title>
    
        <script src="js/prefixfree.min.js"></script>
        <script src="js/LocalFunctions/create.js"></script>

        <script text="type/javascript">
        	window.onload = function() {
        		loadCompanies("companyId");
        	}

        </script>

	    <link rel="stylesheet" type="text/css" href="css/style.css">

	    <style type="text/css">

	    	.login input[type=submit]{
				width: 260px;
				height: 35px;
				background: #fff;
				border: 1px solid #fff;
				cursor: pointer;
				border-radius: 2px;
				color: #a18d6c;
				font-family: 'Exo', sans-serif;
				font-size: 16px;
				font-weight: 400;
				padding: 6px;
				margin-top: 10px;
			}
		</style>

    
  </head>

  <body>

	    <div class="body"></div>

	    	<div class="header">
				<div>Crear<span>Usuario</span></div>
			</div>

			<div class="grad"></div>

			<br>
			<form action="../Endpoints/Create_User_Endpoint.php" method="post">
				<div class="login">
						<input type="text" placeholder="Usuario" name="user" required><br><br>
						<select class="select-style" name="type">
							<option value ="ADMIN">Administrador</option>
							<option value ="NORMAL">Normal</option>
						</select><br>
						<input type="password" placeholder="Password" name="pass" required><br>
						<input type="password" placeholder="Confirmar Password" name="Cpass" required><br><br>
						<select class="select-style" name="companyId" id="companyId"></select><br>
						<input type="submit" value="Crear">
				</div>
			</form>
	    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>    
       
  </body>
</html>
