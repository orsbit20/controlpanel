loadLeagues = function(messageWarning, messageWarning2, selectId, selectId2, buttonId) {

	$.ajax({
		url: '../Endpoints/Get_All_Leagues.php?t=' + Math.random(),
		dataType: 'json',
		type: 'get',
		success: function(data){

					var selectElement = document.getElementById(selectId);

					selectElement.options.length = 0;

					if(data.length > 0) {

						var leagueId;

						data.forEach(function(league, index) {
							league = JSON.parse(league);
							var optionElement = document.createElement("option");

							optionElement.value = league.id;
							optionElement.innerHTML = league.name;

							if(index == 0) {
								leagueId = league.id;
							}

							selectElement.appendChild(optionElement);
						});

						loadTeams(messageWarning2, selectId2, buttonId, leagueId);

					}else {
						document.getElementById(buttonId).disabled = true;
						document.getElementById(messageWarning).hidden = false;
					}
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}

loadTeams = function(messageWarning, selectId, buttonId, leagueId) {

	$.ajax({
		url: '../Endpoints/Get_All_Teams_By_League.php?t=' + Math.random()+'&league='+leagueId,
		dataType: 'json',
		type: 'get',
		success: function(data){

					var selectElement = document.getElementById(selectId);

					selectElement.options.length = 0;

					if(data.length > 0) {

						data.forEach(function(team, index) {
							team = JSON.parse(team);
							var optionElement = document.createElement("option");

							optionElement.value = team.id;
							optionElement.innerHTML = team.name;

							selectElement.appendChild(optionElement);

							document.getElementById(buttonId).disabled = false;
						});
					}else {
						document.getElementById(buttonId).disabled = true;
						document.getElementById(messageWarning).hidden = false;
					}
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}