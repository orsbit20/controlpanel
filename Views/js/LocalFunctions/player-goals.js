loadLeagues = function(messageWarning, selectId, datatableId) {

	$.ajax({
		url: '../Endpoints/Get_All_Leagues.php?t=' + Math.random(),
		dataType: 'json',
		type: 'get',
		success: function(data){

					var selectElement = document.getElementById(selectId);

					selectElement.options.length = 0;

					var leagueId;

					if(data.length > 0) {

						data.forEach(function(league, index) {
							league = JSON.parse(league);
							var optionElement = document.createElement("option");

							optionElement.value = league.id;
							optionElement.innerHTML = league.name;

							if(index == 0) {
								leagueId = league.id;
							}

							selectElement.appendChild(optionElement);
						});

						$("#"+selectId).on("change", function() {
                			loadGoals("dataTables-goals", this.value);
            			});

						loadGoals(datatableId, leagueId);

					}else {
						document.getElementById(messageWarning).hidden = false;
					}
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}

loadGoals = function(datatableId, leagueId) {
	$.ajax({
		url: '../Endpoints/Get_Table_Goals.php?t=' + Math.random() + '&league=' + leagueId,
		dataType: 'json',
		type: 'get',
		success: function(data){
					var datatable = $('#'+datatableId).dataTable();

					datatable.fnClearTable();

					data.forEach(function(player, index) {
						player = JSON.parse(player);

						var logo = createImageElement(createBlobImage(player.photo));

						datatable.fnAddData([ logo, player.name, player.team, player.goals]);
					});
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}