loadLeagues = function(messageWarning, selectId, datatableId) {

	$.ajax({
		url: '../Endpoints/Get_All_Leagues.php?t=' + Math.random(),
		dataType: 'json',
		type: 'get',
		success: function(data){

					var selectElement = document.getElementById(selectId);

					selectElement.options.length = 0;

					var leagueId;

					if(data.length > 0) {

						data.forEach(function(league, index) {
							league = JSON.parse(league);
							var optionElement = document.createElement("option");

							optionElement.value = league.id;
							optionElement.innerHTML = league.name;

							if(index == 0) {
								leagueId = league.id;
							}

							selectElement.appendChild(optionElement);
						});

						$("#"+selectId).on("change", function() {
                			loadPlayers(datatableId, this.value);
            			});

						loadPlayers(datatableId, leagueId);

					}else {
						document.getElementById(messageWarning).hidden = false;
					}
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}

loadPlayers = function(datatable, leagueId) {

	$.ajax({
		url: '../Endpoints/Get_All_Players_By_League.php?t=' + Math.random() + '&league=' + leagueId,
		dataType: 'json',
		type: 'get',
		success: function(data){

					datatable = $('#'+datatable).dataTable();

					datatable.fnClearTable();

					data.forEach(function(player, index) {
						player = JSON.parse(player);
						var imageElement = createImageElement(createBlobImage(player.photo));
						datatable.fnAddData([ player.league_Name, player.team, player.name, player.number, imageElement]);
					});
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}
