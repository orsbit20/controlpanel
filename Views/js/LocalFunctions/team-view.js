loadLeagues = function(messageWarning, selectId, datatableId) {

	$.ajax({
		url: '../Endpoints/Get_All_Leagues.php?t=' + Math.random(),
		dataType: 'json',
		type: 'get',
		success: function(data){

					var selectElement = document.getElementById(selectId);

					selectElement.options.length = 0;

					var leagueId;

					if(data.length > 0) {

						data.forEach(function(league, index) {
							league = JSON.parse(league);
							var optionElement = document.createElement("option");

							optionElement.value = league.id;
							optionElement.innerHTML = league.name;

							if(index == 0) {
								leagueId = league.id;
							}

							selectElement.appendChild(optionElement);
						});

						$("#"+selectId).on("change", function() {
                			loadTeams(datatableId, this.value);
            			});

						loadTeams(datatableId, leagueId);

					}else {
						document.getElementById(messageWarning).hidden = false;
					}
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}

loadTeams = function(datatable, leagueId) {

	$.ajax({
		url: '../Endpoints/Get_All_Teams_By_League.php?t=' + Math.random() + '&league=' + leagueId,
		dataType: 'json',
		type: 'get',
		success: function(data){
					datatable = $('#'+datatable).dataTable();
					datatable.fnClearTable();
					data.forEach(function(team, index) {
						team = JSON.parse(team);
						var imageElement = createImageElement(createBlobImage(team.logo));
						datatable.fnAddData([ imageElement, team.league, team.name, team.description, team.contact_Name, team.phone, team.registry_Date]);
					});
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}