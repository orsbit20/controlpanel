var teamId;
var playerId;


loadLeagues = function(messageWarning, selectId, datatableId) {

	$.ajax({
		url: '../Endpoints/Get_All_Leagues.php?t=' + Math.random(),
		dataType: 'json',
		type: 'get',
		success: function(data){

					var selectElement = document.getElementById(selectId);

					selectElement.options.length = 0;

					var leagueId;

					if(data.length > 0) {

						data.forEach(function(league, index) {
							league = JSON.parse(league);
							var optionElement = document.createElement("option");

							optionElement.value = league.id;
							optionElement.innerHTML = league.name;

							if(index == 0) {
								leagueId = league.id;
							}

							selectElement.appendChild(optionElement);
						});

						$("#"+selectId).on("change", function() {
                			loadMatches("dataTables-matches", this.value);
            			});

						loadMatches(datatableId, leagueId);

					}else {
						document.getElementById(messageWarning).hidden = false;
					}
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}

loadMatches = function(datatableId, leagueId) {
	$.ajax({
		url: '../Endpoints/Get_All_Matches_By_League.php?t=' + Math.random() + '&league=' + leagueId,
		dataType: 'json',
		type: 'get',
		success: function(data){
					var datatable = $('#'+datatableId).dataTable();

					datatable.fnClearTable();

					data.forEach(function(match, index) {
						match = JSON.parse(match);

						var localLogo = createImageWithTextElement(createBlobImage(match.local_Logo), match.local_Team);
						var foreignLogo = createImageWithTextElement(createBlobImage(match.foreign_Logo), match.foreign_Team);
						var customButton = createCustomButton(match.id, "Agregar Resultado");

						datatable.fnAddData([ match.number, localLogo, foreignLogo, match.court, match.referee, match.date, customButton]);

						var buttonElement = document.getElementById(match.id);
						buttonElement.onclick = function() {
							var inputHidden = document.getElementById("matchIdHide");
							inputHidden.value = this.id;

							var mainResults = document.getElementById("mainResults");
							mainResults.className = "openContent";
						};
					});
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}

loadTeams = function(match, selectTeamId, selectPlayerId ) {
	match = document.getElementById(match).value;

	$.ajax({
		url: '../Endpoints/Get_Match_Teams.php?t=' + Math.random() + "&match=" + match,
		dataType: 'json',
		type: 'get',
		success: function(data){

					var selectElement = document.getElementById(selectTeamId);

					selectElement.options.length = 0;

					var teamId;

					data.forEach(function(team, index) {
						team = JSON.parse(team);
						var optionElement = document.createElement("option");

						optionElement.value = team.id;
						optionElement.innerHTML = team.name;

						if(index == 0) {
							teamId = team.id;
						}

						selectElement.appendChild(optionElement);
					});

					$("#"+selectTeamId).on("change", function() {
                		loadPlayers(selectPlayerId, this.value);
            		});

					loadPlayers(selectPlayerId, teamId);
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}

loadPlayers = function(selectId, teamId) {
	$.ajax({
		url: '../Endpoints/Get_All_Players_By_Team.php?t=' + Math.random() + "&team=" + teamId,
		dataType: 'json',
		type: 'get',
		success: function(data){

					var selectElement = document.getElementById(selectId);

					selectElement.options.length = 0;

					data.forEach(function(player, index) {
						player = JSON.parse(player);
						var optionElement = document.createElement("option");

						optionElement.value = player.id;
						optionElement.innerHTML = player.name;

						selectElement.appendChild(optionElement);
					});
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}

createCustomButton = function(id, name) {
	return "<button id='"+ id +"' type='button' class='btn btn-default'>Crear</button>";
}

createResultDetail = function(idContent, idSubPanel) {
	var mainContent = document.getElementById(idContent);

	var column = document.createElement("div");
	column.className = "col-lg-12";

	mainContent.appendChild(column);

	var buttonDiv = document.createElement("div");
	buttonDiv.align = "right";

	var buttonControl = document.createElement("button");
	buttonControl.className = "btn btn-default";
	buttonControl.type = "button";
	buttonControl.id = "b"+idSubPanel;
	buttonControl.innerHTML = "Cerrar";
	buttonControl.onclick = function() {

		if(this.innerHTML == "Cerrar") {
			this.innerHTML = "Abrir";
			var divId = this.id.substring(1, this.id.length);

			var divContent = document.getElementById(divId);

			divContent.className = "panel-body closeContent";
		}else{
			this.innerHTML = "Cerrar";
			var divId = this.id.substring(1, this.id.length);

			var divContent = document.getElementById(divId);

			divContent.className = "panel-body openContent";
		}
	}

	buttonDiv.appendChild(buttonControl);

	var panel = document.createElement("div");
	panel.className = "panel panel-default";

	column.appendChild(panel);

	var panelHeading = document.createElement("div");
	panelHeading.className = "panel-heading";
	panelHeading.innerHTML = "Detalle de resultados";

	var panelBody = document.createElement("div");
	panelBody.className = "panel-body openContent";
	panelBody.id = idSubPanel;

	panel.appendChild(buttonDiv);
	panel.appendChild(panelHeading);
	panel.appendChild(panelBody);

	createPanelBody(panelBody);

	loadTeams("matchIdHide", teamId, playerId);
}

createPanelBody = function(panelBody) {
	panelBody = createFirstRow(panelBody);
	panelBody = createSecondRow(panelBody);
	panelBody = createThirdRow(panelBody);
}

createFirstRow = function(panelBody) {
	var row = document.createElement("div");
	row.className = "row";

	teamId = "selectTeam" + Math.floor((Math.random() * 1000) + 1);
	var selectElement = document.createElement("select");
	selectElement.className = "form-control";
	selectElement.id = teamId;

	row.appendChild(createFormGroup("col-lg-6", "Equipos", selectElement));

	panelBody.appendChild(row);

	return panelBody;
}

createSecondRow = function(panelBody) {
	var row2 = document.createElement("div");
	row2.className = "row";

	playerId = "selectPlayer" + Math.floor((Math.random() * 1000) + 1);
	var selectTeam = document.createElement("select");
	selectTeam.className = "form-control";
	selectTeam.id = playerId;
	selectTeam.name = "player[]";

	var selectYellow = document.createElement("input");
	selectYellow.className = "form-control";
	selectYellow.name = "yellow[]";
	selectYellow.type = "number";
	selectYellow.max = "3";
	selectYellow.min = "0";

	var inputGoals = document.createElement("input");
	inputGoals.className = "form-control";
	inputGoals.name = "goals[]";
	inputGoals.type = "number";
	inputGoals.max = "100";
	inputGoals.min = "0";

	var checkboxDiv = document.createElement("div");
	checkboxDiv.className = "checkbox";

	var checkboxLabel = document.createElement("label");

	var checkboxInput = document.createElement("input");

	checkboxLabel.appendChild(checkboxInput);

	checkboxInput.type = "checkbox";
	checkboxInput.value = "false";
	checkboxInput.name = "red[]";

	checkboxDiv.appendChild(checkboxLabel);

	row2.appendChild(createFormGroup("col-lg-3", "Jugadores", selectTeam));
	row2.appendChild(createFormGroup("col-lg-3", "Goles", inputGoals));
	row2.appendChild(createFormGroup("col-lg-3", "Amarillas", selectYellow));
	row2.appendChild(createFormGroup("col-lg-3", "Roja?", checkboxDiv));

	panelBody.appendChild(row2);

	return panelBody;
}

createThirdRow = function(panelBody) {
	var row3 = document.createElement("div");
	row3.className = "row";

	var textAreaElement = document.createElement("textarea");
	textAreaElement.className = "form-control";
	textAreaElement.rows = "3";
	textAreaElement.name = "extra2[]"

	row3.appendChild(createFormGroup("col-lg-12", "Extra", textAreaElement));

	panelBody.appendChild(row3);

	return panelBody;
}

createFormGroup = function(size, label, control) {
	var col = document.createElement("div");
	col.className = size;

	var formGroup = document.createElement("div");
	formGroup.className = "form-group";

	var labelControl = document.createElement("label");
	labelControl.innerHTML = label;

	formGroup.appendChild(labelControl);
	formGroup.appendChild(control);

	col.appendChild(formGroup);

	return col;
}

