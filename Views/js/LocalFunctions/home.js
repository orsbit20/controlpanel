loadNews = function(galleria) {

	$.ajax({
		url: '../Endpoints/Get_All_News_By_Company.php?t=' + Math.random(),
		dataType: 'json',
		type: 'get',
		success: function(data){
					data.forEach(function(news, index) {
						news = JSON.parse(news);
						generateNews(news, galleria);
					});
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});

}

generateNews = function(news, galleria) {
	var srcImage = createBlobImage(news.photo);

	$("#"+galleria).append("<a href='"+ srcImage +"'><img src='"+ srcImage +"' data-big='" + 
		srcImage + "' data-title='"+ news.title +"' data-description='"+ news.content +"' /></a>");
}
