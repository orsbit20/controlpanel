var teams = new Array();

loadCourts = function(messageWarning, selectId, buttonId) {

	$.ajax({
		url: '../Endpoints/Get_All_Courts_By_Company.php?t=' + Math.random(),
		dataType: 'json',
		type: 'get',
		success: function(data){

					var selectElement = document.getElementById(selectId);

					selectElement.options.length = 0;

					if(data.length > 0) {

						data.forEach(function(court, index) {
							court = JSON.parse(court);
							var optionElement = document.createElement("option");

							optionElement.value = court.id;
							optionElement.innerHTML = court.name;

							selectElement.appendChild(optionElement);
						});

					}else {
						document.getElementById(buttonId).disabled = true;
						document.getElementById(messageWarning).hidden = false;
					}
				}, 
		error: function(error){ 
					document.getElementById(buttonId).disabled = true;
					console.log(error.responseText); 
				} 
	});
}

loadReferees = function(messageWarning, selectId, buttonId) {

	$.ajax({
		url: '../Endpoints/Get_All_Referee.php?t=' + Math.random(),
		dataType: 'json',
		type: 'get',
		success: function(data){

					var selectElement = document.getElementById(selectId);

					selectElement.options.length = 0;

					if(data.length > 0) {

						data.forEach(function(referee, index) {
							referee = JSON.parse(referee);
							var optionElement = document.createElement("option");

							optionElement.value = referee.id;
							optionElement.innerHTML = referee.name;

							selectElement.appendChild(optionElement);
						});

					}else {
						document.getElementById(buttonId).disabled = true;
						document.getElementById(messageWarning).hidden = false;
					}
				}, 
		error: function(error){ 
					document.getElementById(buttonId).disabled = true;
					console.log(error.responseText); 
				} 
	});
}

loadLeagues = function(messageWarningLeague, messageWarningTeam, messageWarningOne, selectId, selectIdLocal, selectIdForeign, buttonId) {

	$.ajax({
		url: '../Endpoints/Get_All_Leagues.php?t=' + Math.random(),
		dataType: 'json',
		type: 'get',
		success: function(data){

					var selectElement = document.getElementById(selectId);

					selectElement.options.length = 0;

					if(data.length > 0) {

						var leagueId;

						data.forEach(function(league, index) {
							league = JSON.parse(league);
							var optionElement = document.createElement("option");

							optionElement.value = league.id;
							optionElement.innerHTML = league.name;

							if(index == 0) {
								leagueId = league.id;
							}

							selectElement.appendChild(optionElement);
						});

						$("#"+selectId).on("change", function() {
                			loadTeams("messageWarningTeam", "messageWarningOneTeam", "selectLocal", "selectForeign", "createButtonId", this.value);
            			});

						loadTeams(messageWarningTeam, messageWarningOne, selectIdLocal, selectIdForeign, buttonId, leagueId);

					}else {
						document.getElementById(buttonId).disabled = true;
						document.getElementById(messageWarningLeague).hidden = false;
					}
				}, 
		error: function(error){ 
					document.getElementById(buttonId).disabled = true;
					console.log(error.responseText); 
				} 
	});
}

loadTeams = function(messageWarningTeam, messageWarningOne, selectIdLocal, selectIdForeign, buttonId, leagueId) {

	$.ajax({
		url: '../Endpoints/Get_All_Teams_By_League.php?t=' + Math.random()+'&league='+leagueId,
		dataType: 'json',
		type: 'get',
		success: function(data){

					var selectLocalElement = document.getElementById(selectIdLocal);
					var selectForeignElement = document.getElementById(selectIdForeign);

					selectLocalElement.options.length = 0;
					selectForeignElement.options.length = 0;

					if(data.length > 0) {
						if(data.length > 1) {

							teams = new Array();

							data.forEach(function(team, index) {
								team = JSON.parse(team);
								var optionElementL = document.createElement("option");
								var optionElementF = document.createElement("option");

								optionElementL.value = team.id;
								optionElementL.innerHTML = team.name;
								selectLocalElement.appendChild(optionElementL);

								if(index > 0) {
									optionElementF.value = team.id;
									optionElementF.innerHTML = team.name;
									selectForeignElement.appendChild(optionElementF);
								}

								var teamJSON = {id:team.id, name:team.name};

								teams.push(teamJSON);

								document.getElementById(buttonId).disabled = false;
							});

						}else {
							document.getElementById(buttonId).disabled = true;
							document.getElementById(messageWarningOne).hidden = false;
						}
					}else {
						document.getElementById(buttonId).disabled = true;
						document.getElementById(messageWarningTeam).hidden = false;
					}
				}, 
		error: function(error){ 
					document.getElementById(buttonId).disabled = true;
					console.log(error.responseText); 
				} 
	});
}

noDuplicateTeams = function(element) {
	var id = element.id;
	var value = element.value;

	var selectElement;

	if(id.indexOf("Local")) {
		selectElement = document.getElementById("selectForeign");
	}else{
		selectElement = document.getElementById(id);
	}

	selectElement.options.length = 0;

	teams.forEach(function(team, index) {

		var optionElement = document.createElement("option");

		if(value != team.id){
			optionElement.value = team.id;
			optionElement.innerHTML = team.name;

			selectElement.appendChild(optionElement);
		}
	});
}