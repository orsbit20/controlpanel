loadLeagues = function(messageWarning, selectId, buttonId) {

	$.ajax({
		url: '../Endpoints/Get_All_Leagues.php?t=' + Math.random(),
		dataType: 'json',
		type: 'get',
		success: function(data){

					if(data.length > 0) {
						var selectElement = document.getElementById(selectId);

						data.forEach(function(league, index) {
							league = JSON.parse(league);
							var optionElement = document.createElement("option");

							optionElement.value = league.id;
							optionElement.innerHTML = league.name;

							selectElement.appendChild(optionElement);
						});
						document.getElementById(buttonId).disabled = false;
					}else {
						document.getElementById(messageWarning).hidden = false;
					}
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}