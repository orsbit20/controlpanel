loadLeagues = function(datatable) {

	$.ajax({
		url: '../Endpoints/Get_All_Leagues.php?t=' + Math.random(),
		dataType: 'json',
		type: 'get',
		success: function(data){
					datatable.fnClearTable();
					data.forEach(function(league, index) {
						league = JSON.parse(league);
						datatable.fnAddData([ league.name, league.description, league.prices, league.dates, league.day]);
					});
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}