loadReferees = function(datatable) {

	$.ajax({
		url: '../Endpoints/Get_All_Referee.php?t=' + Math.random(),
		dataType: 'json',
		type: 'get',
		success: function(data){
					datatable.fnClearTable();
					data.forEach(function(referee, index) {
						referee = JSON.parse(referee);
						datatable.fnAddData([ referee.name, referee.address, referee.phone, referee.web_Page, referee.rfc ]);
					});
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}