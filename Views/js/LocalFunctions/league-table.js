loadLeagues = function(messageWarning, selectId, datatableId) {

	$.ajax({
		url: '../Endpoints/Get_All_Leagues.php?t=' + Math.random(),
		dataType: 'json',
		type: 'get',
		success: function(data){

					var selectElement = document.getElementById(selectId);

					selectElement.options.length = 0;

					var leagueId;

					if(data.length > 0) {

						data.forEach(function(league, index) {
							league = JSON.parse(league);
							var optionElement = document.createElement("option");

							optionElement.value = league.id;
							optionElement.innerHTML = league.name;

							if(index == 0) {
								leagueId = league.id;
							}

							selectElement.appendChild(optionElement);
						});

						$("#"+selectId).on("change", function() {
                			loadTable("dataTables-table", this.value);
            			});

						loadTable(datatableId, leagueId);

					}else {
						document.getElementById(messageWarning).hidden = false;
					}
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}

loadTable = function(datatableId, leagueId) {
	$.ajax({
		url: '../Endpoints/Get_Table.php?t=' + Math.random() + '&league=' + leagueId,
		dataType: 'json',
		type: 'get',
		success: function(data){
					var datatable = $('#'+datatableId).dataTable();

					datatable.fnClearTable();

					data.forEach(function(row, index) {
						row = JSON.parse(row);

						console.log(row);

						var logo = createImageElement(createBlobImage(row.team));

						datatable.fnAddData([ logo, row.points, row.games, row.won, row.lost, row.drawn, row.favor, row.against]);
					});
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}