loadCourts = function(datatable) {

	$.ajax({
		url: '../Endpoints/Get_All_Courts_By_Company.php?t=' + Math.random(),
		dataType: 'json',
		type: 'get',
		success: function(data){
					datatable.fnClearTable();
					data.forEach(function(court, index) {
						court = JSON.parse(court);
						datatable.fnAddData([ court.name, court.description, court.latitude, court.length]);
					});
				}, 
		error: function(error){ 
					console.log(error.responseText); 
				} 
	});
}