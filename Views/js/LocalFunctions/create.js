
loadCompanies = function(companyId) {
	var loadCompaniesHttp = createRequest();

	loadCompaniesHttp.onreadystatechange = function() {
  		if (loadCompaniesHttp.readyState==4 && loadCompaniesHttp.status==200) {
			companies = JSON.parse(loadCompaniesHttp.responseText);
			
			var selectElement = document.getElementById(companyId);

			companies.forEach(function(company, index) {
				company = JSON.parse(company);

				var optionElement = document.createElement("option");

				optionElement.value = company.id;
				optionElement.innerHTML = company.name;

				selectElement.appendChild(optionElement);
			});
    	}
  	}

	loadCompaniesHttp.open("GET","../Endpoints/Get_Company_Endpoint.php?t=" + Math.random(),true);
	loadCompaniesHttp.send();
}


createRequest = function() {
	var xmlhttp;

	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	return xmlhttp;
}