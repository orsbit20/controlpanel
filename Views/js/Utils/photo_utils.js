createImageElement = function(src) {
	return "<img src='"+src+"' height='60px' width='60px' />";
}

createImageWithTextElement = function(src, altText) {
	return "<img src='"+src+"' height='40px' width='40px' /></br>" + altText;
}

createBlobImage = function(photoBased64) {
	var binaryImg = atob(photoBased64);
	var length = binaryImg.length;
	var buffer = new ArrayBuffer(length);
	var uInt8array = new Uint8Array(buffer);

	for (var i = 0; i < length; i++) {
		uInt8array[i] = binaryImg.charCodeAt(i);
	}

	var blob = new Blob([buffer], {
		type: "image/jpg"
	});

	return URL.createObjectURL(blob);
}